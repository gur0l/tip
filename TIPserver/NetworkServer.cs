﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using TIPlib;

namespace TIPserver
{
    public class NetworkServer
    {
        // Incoming data from the client.  

        public static void StartServer()
        {
            short actualLength;
            BinaryReader reader = new BinaryReader(new FileStream("out.log", FileMode.Open));
            byte[] firstLength;

            // Data buffer for incoming data.  
            byte[] bytes = new Byte[1024];

            // Establish the local endpoint for the socket.  
            // Dns.GetHostName returns the name of the   
            // host running the application.  Dns.Resolve(Dns.GetHostName());
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            bool isConverted = IPAddress.TryParse("127.0.0.1",
                out IPAddress ipAddress);
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 11000);

            // Create a TCP/IP socket.  
            Socket mySocket = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and   
            // listen for incoming connections.  
            try
            {
                mySocket.Bind(localEndPoint);
                mySocket.Listen(10);

                // Start listening for connections.  
                while (true)
                {
                    Console.WriteLine("Waiting for a connection...");

                    // Program is suspended while waiting for an incoming connection.  
                    Socket handler = mySocket.Accept();

                    // Print if connection is successful.
                    Console.WriteLine("Connected.");

                    int count = 0;

                    // An incoming connection needs to be processed.  
                    while (reader.BaseStream.Position != reader.BaseStream.Length)
                    {
                        count++;
                        Console.WriteLine("File Read Count: " + count);

                        firstLength = new byte[2];
                        reader.Read(firstLength, 0, 2);

                        if (BitConverter.IsLittleEndian)
                        {
                            Array.Reverse(firstLength);
                            actualLength = BitConverter.ToInt16(firstLength, 0);
                        }
                        else
                            actualLength = BitConverter.ToInt16(firstLength, 0);

                        Console.WriteLine("Length acquired from file: " + actualLength);

                        // Send the data through the socket.
                        byte[] message = new Byte[actualLength];
                        reader.Read(message, 0, actualLength);
                        
                        byte[] intBytes = BitConverter.GetBytes(actualLength);
                        if (BitConverter.IsLittleEndian)
                            Array.Reverse(intBytes);
                        byte[] lengthResult = intBytes;

                        int bytesSent = handler.Send(lengthResult);

                        bytesSent = handler.Send(message);
                    }

                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            Console.WriteLine("\nPress ENTER to continue...");
            Console.Read();

        }

        public static int Main(String[] args)
        {
            StartServer();
            return 0;
        }
    }
}