﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TIPproject
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] LineArray = TipFeeder.ReadFile("tip_20150730_all.log");

            for (int i = 0; i < LineArray.Length; i++)
                TipMsgGen.generateTipMsg(LineArray[i]);
        }
    }
}
