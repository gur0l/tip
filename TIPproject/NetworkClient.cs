﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using TIPlib;

namespace TIPproject
{
    public class NetworkClient
    {
        public static void StartClient()
        {
            // Data buffer for incoming data.  
            byte[] bytes = new byte[1024];

            // Connect to a remote device.  
            try
            {
                // Establish the remote endpoint for the socket.  
                // This example uses port 11000 on the local computer.  
                IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
                bool isConverted = IPAddress.TryParse("127.0.0.1",
                    out IPAddress ipAddress);
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, 11000);

                // Create a TCP/IP  socket.  
                Socket mySocket = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);

                int count = 0;

                // Connect the socket to the remote endpoint. Catch any errors.  
                try
                {
                    mySocket.Connect(remoteEP);

                    Console.WriteLine("Socket connected to {0}",
                        mySocket.RemoteEndPoint.ToString());

                    // An incoming connection needs to be processed.  
                    while (true)
                    {
                        count++;
                        Console.WriteLine("Listen Count: " + count);

                        bytes = new byte[2];
                        short actualLength;
                        int bytesRec = mySocket.Receive(bytes);

                        if (BitConverter.IsLittleEndian)
                        {
                            Array.Reverse(bytes);
                            actualLength = BitConverter.ToInt16(bytes, 0);
                        }
                        else
                            actualLength = BitConverter.ToInt16(bytes, 0);

                        Console.WriteLine("Received Length Value: " + actualLength);

                        byte[] messageRec = new byte[actualLength];
                        bytesRec = mySocket.Receive(messageRec);

                        string result = Encoding.ASCII.GetString(messageRec);
                        
                        Console.WriteLine("Received Message: " + result);
                        Console.WriteLine("Received Message Length: " + result.Length);

                        //result.TrimStart('S');

                        //TipMsg TIPmessage = TipMsgGen.generateTipMsg(result);

                        // Show the data on the console.  
                        //Console.WriteLine("Text received : {0}", result);

                        if (count == 116705)
                            break;
                    }

                    // Release the socket.  
                    mySocket.Shutdown(SocketShutdown.Both);
                    mySocket.Close();

                }
                catch (ArgumentNullException ane)
                {
                    Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
                }
                catch (SocketException se)
                {
                    Console.WriteLine("SocketException : {0}", se.ToString());
                }
                catch (Exception e)
                {
                    Console.WriteLine("Unexpected exception : {0}", e.ToString());
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            Console.Read();
        }

        public static int Main(String[] args)
        {
            StartClient();
            return 0;
        }
    }
}