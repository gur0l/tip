﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataParticipantTest
    {
        [Test]
        public void generateBasicDataParticipant()
        {
            BasicDataParticipant BDp = new BasicDataParticipant
            (
                "BDp;i2384;SiBITES;s1;SYmBITES;NAmTEKSTIL BANKASI A.S.;PAt2164;BIcTEKBTRISXXX;GRsN;"
            );

            Assert.AreEqual(2384, BDp.ID);
            Assert.AreEqual("BITES", BDp.sourceID);
            Assert.AreEqual(1, BDp.sourceSystem);
            Assert.AreEqual("BITES", BDp.symbol);
            Assert.AreEqual("TEKSTIL BANKASI A.S.", BDp.name);
            Assert.AreEqual(2164, BDp.participantType);

            if (BDp.bicFlag == true)
                Assert.AreEqual("TEKBTRISXXX", BDp.bicCode);

            Assert.AreEqual('N', BDp.grossSettlement);
        }
    }
}