﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataRightTest
    {
        [Test]
        public void generateBasicDataRight()
        {
            BasicDataRight BDRi = new BasicDataRight
            (
                "BDRi;i1884;SiGARAN.TR;s1;EXcTRY;"
            );

            Assert.AreEqual(1884, BDRi.ID);
            Assert.AreEqual("GARAN.TR", BDRi.sourceID);
            Assert.AreEqual(1, BDRi.sourceSystem);

            if (BDRi.contractSizeFlag)
                Assert.AreEqual(0, BDRi.contractSize);

            DateTime testDate = new DateTime(2064, 07, 14);

            if (BDRi.exerciseFromDateFlag)
                Assert.AreEqual(testDate, BDRi.exerciseFromDate);

            if (BDRi.exerciseToDateFlag)
                Assert.AreEqual(testDate, BDRi.exerciseToDate);

            if (BDRi.totalIssueFlag)
                Assert.AreEqual(0, BDRi.totalIssue);

            Assert.AreEqual("TRY", BDRi.exerciseCurrency);

            if (BDRi.hotInsertedFlag)
                Assert.AreEqual(true, BDRi.hotInserted);
        }
    }
}