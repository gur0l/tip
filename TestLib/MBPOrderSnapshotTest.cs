﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class MBPOrderSnapshotTest
    {
        [Test]
        public void generateMBPOrderSnapshot()
        {
            MBPOrderSnapshot k = new MBPOrderSnapshot
            (
                "k;s1;i1706;t173502.228;RBp1:2.7;RBv1:1000;RAp1:2.77;RAv1:750;"
            );

            Assert.AreEqual(1, k.sourceSystem);
            Assert.AreEqual(1706, k.ID);
            Assert.AreEqual("17:35:02.228", k.timeExec);

            if (k.rankedBidPriceFlag)
            {
                Assert.AreEqual(2.7, k.RBp[0]);
            }

            if (k.rankedBidVolumeFlag)
            {
                Assert.AreEqual(1000, k.RBv[0]);
            }

            if (k.rankedAskPriceFlag)
            {
                Assert.AreEqual(2.77, k.RAp[0]);
            }

            if (k.rankedAskVolumeFlag)
            {
                Assert.AreEqual(750, k.RAv[0]);
            }
        }
    }
}