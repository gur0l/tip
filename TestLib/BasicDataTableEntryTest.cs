﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataTableEntryTest
    {
        [Test]
        public void generateBasicDataTableEntry()
        {
            BasicDataTableEntry BDTe = new BasicDataTableEntry
            (
                "BDTe;i22;Si01;s1;TEt2;SYm01;NAmR.H. Kullandirilarak Bedelli Sermaye Art;"
            );

            Assert.AreEqual(22, BDTe.ID);
            Assert.AreEqual("01", BDTe.sourceID);
            Assert.AreEqual(1, BDTe.sourceSystem);
            Assert.AreEqual(2, BDTe.tableEntryType);
            Assert.AreEqual("01", BDTe.symbol);
            Assert.AreEqual("R.H. Kullandirilarak Bedelli Sermaye Art", BDTe.name);
        }
    }
}