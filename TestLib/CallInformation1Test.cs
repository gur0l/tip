﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class CallInformation1Test
    {
        [Test]
        public void generateCallInformation1()
        {
            CallInformation1 c = new CallInformation1
            (
                "c;i1622;s1;t095011.094;EQp23.98;EQv950;"
            );

            Assert.AreEqual(1622, c.ID);
            Assert.AreEqual(1, c.sourceSystem);
            Assert.AreEqual("09:50:11.094", c.timeExec);
            Assert.AreEqual(23.98, c.equilibriumPrice);
            Assert.AreEqual(950, c.equilibriumVolume);

            if (c.orderbookFlushFlag)
                Assert.AreEqual(true, c.orderbookFlush);
        }
    }
}