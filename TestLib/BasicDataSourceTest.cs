﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataSourceTest
    {
        [Test]
        public void generateBasicDataSource()
        {
            BasicDataSource BDSr = new BasicDataSource("BDSr;i1;NAmGITS;");

            Assert.AreEqual(1, BDSr.ID);
            Assert.AreEqual("GITS", BDSr.sourceData);
        }
    }
}
