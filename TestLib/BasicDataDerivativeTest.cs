﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataDerivativeTest
    {
        [Test]
        public void generateBasicDataDerivative()
        {
            BasicDataDerivative BDDe = new BasicDataDerivative
            (
                "BDDe;i396;SiGARIYMEPW999.MV;s1;DTy1;DXt2;STp6;CSz1;SEt1;EXe20640714;"
            );

            Assert.AreEqual(396, BDDe.ID);
            Assert.AreEqual("GARIYMEPW999.MV", BDDe.sourceID);
            Assert.AreEqual(1, BDDe.sourceSystem);
            Assert.AreEqual(1, BDDe.derivativeType);

            if (BDDe.totalIssueFlag)
                Assert.AreEqual(0, BDDe.totalIssue);

            Assert.AreEqual(2, BDDe.exerciseType);

            if (BDDe.strikePriceFlag)
                Assert.AreEqual(6, BDDe.strikePrice);

            Assert.AreEqual(1, BDDe.contractSize);
            Assert.AreEqual(1, BDDe.settlementType);

            DateTime testDate = new DateTime(2064, 07, 14);

            if (BDDe.exerciseFromDateFlag)
                Assert.AreEqual(testDate, BDDe.exerciseFromDate);

            Assert.AreEqual(testDate, BDDe.exerciseToDate);

            if (BDDe.settlementDateFlag)
                Assert.AreEqual(testDate, BDDe.settlementDate);

            if (BDDe.hotInsertedFlag)
                Assert.AreEqual(true, BDDe.hotInserted);
        }
    }
}