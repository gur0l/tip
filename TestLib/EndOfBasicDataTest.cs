﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class EndOfBasicDataTest
    {
        [Test]
        public void generateEndOfBasicData()
        {
            EndOfBasicData EOBd = new EndOfBasicData
            (
                "EOBd;s1;"
            );

            Assert.AreEqual(1, EOBd.sourceSystem);
        }
    }
}