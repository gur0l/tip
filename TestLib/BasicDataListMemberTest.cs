﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataListMemberTest
    {
        [Test]
        public void generateBasicDataListMember()
        {
            BasicDataListMember BDLm = new BasicDataListMember
            (
                "BDLm;IDo1044;LSi38;s1;"
            );

            Assert.AreEqual(1044, BDLm.orderbookID);
            Assert.AreEqual(38, BDLm.listID);
            Assert.AreEqual(1, BDLm.sourceSystem);

            if (BDLm.hotInsertedFlag)
            {
                Assert.AreEqual(true, BDLm.hotInserted);
            }
        }
    }
}