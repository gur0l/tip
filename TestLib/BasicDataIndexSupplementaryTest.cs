﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataIndexSupplementaryTest
    {
        [Test]
        public void generateBasicDataIndexSupplementary()
        {
            BasicDataIndexSupplementary BDIp = new BasicDataIndexSupplementary
            (
                "BDIp;i2548;SiXUTUM;s2;ISnTRAIMKB00044;"
            );

            Assert.AreEqual(2548, BDIp.ID);
            Assert.AreEqual("XUTUM", BDIp.sourceID);
            Assert.AreEqual(2, BDIp.sourceSystem);
            Assert.AreEqual("TRAIMKB00044", BDIp.ISIN);
        }
    }
}