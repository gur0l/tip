﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataTradableSupplementaryTest
    {
        [Test]
        public void generateBasicDataTradableSupplementary()
        {
            BasicDataTradableSupplementary BDTr = new BasicDataTradableSupplementary
            (
                "BDTr;i1156;SiGARAN.HE;s1;ISnTRAGARAN91N1;"
            );

            Assert.AreEqual(1156, BDTr.ID);
            Assert.AreEqual("GARAN.HE", BDTr.sourceID);
            Assert.AreEqual(1, BDTr.sourceSystem);
            Assert.AreEqual("TRAGARAN91N1", BDTr.ISIN);
        }
    }
}