﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataUnderlyingInfoTest
    {
        [Test]
        public void generateBasicDataUnderlyingInfo()
        {
            BasicDataUnderlyingInfo BDUi = new BasicDataUnderlyingInfo
            (
                "BDUi;i396;SiGARIYMEPW999.MV;s1;DScEREGLI DEMIR CELIK;"
            );

            Assert.AreEqual(396, BDUi.ID);
            Assert.AreEqual("GARIYMEPW999.MV", BDUi.sourceID);
            Assert.AreEqual(1, BDUi.sourceSystem);

            if (BDUi.underlyingIdFlag)
                Assert.AreEqual(0, BDUi.underlyingId);

            Assert.AreEqual("EREGLI DEMIR CELIK", BDUi.description);

            if (BDUi.hotInsertedFlag)
                Assert.AreEqual(true, BDUi.hotInserted);
        }
    }
}