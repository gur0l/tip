﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class OrderbookSummaryTest
    {
        [Test]
        public void generateOrderbookSummary()
        {
            OrderbookSummary m = new OrderbookSummary
            (
                "m;i388;s1;t174000.124;Dt20150729;ISOcY;ISOtY;Pl57.05;HPm65.5;" +
                "HPMd20150714;LPm56;LPMd20150724;HPy96.7;HPYd20150113;LPy53.55;" +
                "LPYd20150608;LPd20150728;"
            );

            Assert.AreEqual(388, m.ID);
            Assert.AreEqual(1, m.sourceSystem);
            Assert.AreEqual("17:40:00.124", m.timeExec);

            DateTime dateTest = new DateTime(2015, 07, 29);
            DateTime highPriceMonthDateTest = new DateTime(2015, 07, 14);
            DateTime lowPriceMonthDateTest = new DateTime(2015, 07, 24);
            DateTime highPriceYearDateTest = new DateTime(2015, 01, 13);
            DateTime lowPriceYearDateTest = new DateTime(2015, 06, 08);
            DateTime lastPaidDateTest = new DateTime(2015, 07, 28);

            Assert.AreEqual(dateTest, m.date);

            Assert.AreEqual('Y', m.officialClosingPrice);
            Assert.AreEqual('Y', m.officialClosingTurnover);

            //optional part.
            if (m.bidPriceDiffFlag)
            {
                Assert.AreEqual(0, m.bidPriceDiff);
            }

            if (m.bidPriceAtLevel1Flag)
            {
                Assert.AreEqual(0, m.bidPriceAtLevel1);
            }

            if (m.askPriceAtLevel1Flag)
            {
                Assert.AreEqual(0, m.askPriceAtLevel1);
            }

            if (m.firstPriceFlag)
            {
                Assert.AreEqual(0, m.firstPrice);
            }

            if (m.lastPriceFlag)
            {
                Assert.AreEqual(57.05, m.lastPrice);
            }

            if (m.highPriceFlag)
            {
                Assert.AreEqual(0, m.highPrice);
            }

            if (m.lowPriceFlag)
            {
                Assert.AreEqual(0, m.lowPrice);
            }

            if (m.diffLastPriceFlag)
            {
                Assert.AreEqual(0, m.diffLastPrice);
            }

            if (m.numberOfTradesFlag)
            {
                Assert.AreEqual(0, m.numberOfTrades);
            }

            if (m.accumulatedVolumeFlag)
            {
                Assert.AreEqual(0, m.accumulatedVolume);
            }

            if (m.reportedVolumeFlag)
            {
                Assert.AreEqual(0, m.reportedVolume);
            }

            if (m.accumulatedTurnoverFlag)
            {
                Assert.AreEqual(0, m.accumulatedTurnover);
            }

            if (m.reportedTurnoverFlag)
            {
                Assert.AreEqual(0, m.reportedTurnover);
            }

            if (m.settlementPriceFlag)
            {
                Assert.AreEqual(0, m.settlementPrice);
            }

            if (m.highPriceMonthFlag)
            {
                Assert.AreEqual(65.5, m.highPriceMonth);
            }

            if (m.highPriceMonthDateFlag)
            {
                Assert.AreEqual(highPriceMonthDateTest, m.highPriceMonthDate);
            }

            if (m.lowPriceMonthFlag)
            {
                Assert.AreEqual(56, m.lowPriceMonth);
            }

            if (m.lowPriceMonthDateFlag)
            {
                Assert.AreEqual(lowPriceMonthDateTest, m.lowPriceMonthDate);
            }

            if (m.highPriceYearFlag)
            {
                Assert.AreEqual(96.7, m.highPriceYear);
            }

            if (m.highPriceYearDateFlag)
            {
                Assert.AreEqual(highPriceYearDateTest, m.highPriceYearDate);
            }

            if (m.lowPriceYearFlag)
            {
                Assert.AreEqual(53.55, m.lowPriceYear);
            }

            if (m.lowPriceYearDateFlag)
            {
                Assert.AreEqual(lowPriceYearDateTest, m.lowPriceYearDate);
            }

            DateTime lastTradedDateTest = new DateTime(0001, 01, 01);

            if (m.lastTradedDateFlag)
            {
                Assert.AreEqual(lastTradedDateTest, m.lastTradedDate);
            }

            if (m.lastPaidDateFlag)
            {
                Assert.AreEqual(lastPaidDateTest, m.lastPaidDate);
            }

            if (m.closingVWAPFlag)
            {
                Assert.AreEqual(0, m.closingVWAP);
            }
        }
    }
}