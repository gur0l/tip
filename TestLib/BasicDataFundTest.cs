﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataFundTest
    {
        [Test]
        public void generateBasicDataFund()
        {
            BasicDataFund BDEt = new BasicDataFund
            (
                "BDEt;i1250;SiDJIST.F;s1;"
            );

            Assert.AreEqual(1250, BDEt.ID);
            Assert.AreEqual("DJIST.F", BDEt.sourceID);
            Assert.AreEqual(1, BDEt.sourceSystem);

            if (BDEt.hotInsertedFlag)
                Assert.AreEqual(true, BDEt.hotInserted);
        }
    }
}