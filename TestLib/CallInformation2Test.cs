﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class CallInformation2Test
    {
        [Test]
        public void generateCallInformation2()
        {
            CallInformation2 Cl = new CallInformation2
            (
                "Cl;i1672;s1;t171405.548;EQp39;EQv72000;EBv0;EAv58000;"
            );

            Assert.AreEqual(1672, Cl.ID);
            Assert.AreEqual(1, Cl.sourceSystem);
            Assert.AreEqual("17:14:05.548", Cl.timeExec);
            Assert.AreEqual(39, Cl.equilibriumPrice);
            Assert.AreEqual(72000, Cl.equilibriumVolume);
            Assert.AreEqual(0, Cl.remainingBidVolumeAtEPLevel);
            Assert.AreEqual(58000, Cl.remainingAskVolumeAtEPLevel);

            if (Cl.orderbookFlushFlag)
                Assert.AreEqual(true, Cl.orderbookFlush);
        }
    }
}