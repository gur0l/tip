﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataTickSizeTableTest
    {
        [Test]
        public void generateBasicDataTickSizeTable()
        {
            BasicDataTickSizeTable BDTs = new BasicDataTickSizeTable
            (
                "BDTs;i68;SiEQ_WR;s1;NAmEQ_WR;"
            );

            Assert.AreEqual(68, BDTs.ID);
            Assert.AreEqual("EQ_WR", BDTs.sourceID);
            Assert.AreEqual(1, BDTs.sourceSystem);
            Assert.AreEqual("EQ_WR", BDTs.name);
        }
    }
}