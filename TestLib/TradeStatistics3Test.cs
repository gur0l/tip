﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class TradeStatistics3Test
    {
        [Test]
        public void generateTradeStatistics3()
        {
            TradeStatistics3 w = new TradeStatistics3
            (
                "w;i1930;s1;t111828.000;Pf8.15;Pl8.05;" +
                "Ph8.15;LOp8.05;q3;o1300;f10490;Wp8.069;Wd0;Qr0;Lv1000;"
            );

            Assert.AreEqual(1930, w.ID);
            Assert.AreEqual(1, w.sourceSystem);
            Assert.AreEqual("11:18:28.000", w.timeExec);

            //optional part.
            if (w.firstPriceFlag)
            {
                Assert.AreEqual(8.15, w.firstPrice);
            }

            if (w.lastPriceFlag)
            {
                Assert.AreEqual(8.05, w.lastPrice);
            }

            if (w.highPriceFlag)
            {
                Assert.AreEqual(8.15, w.highPrice);
            }

            if (w.lowPriceFlag)
            {
                Assert.AreEqual(8.05, w.lowPrice);
            }

            if (w.diffLastPriceFlag)
            {
                Assert.AreEqual(0, w.diffLastPrice);
            }

            if (w.numberOfTradesFlag)
            {
                Assert.AreEqual(3, w.numberOfTrades);
            }

            if (w.accumulatedVolumeFlag)
            {
                Assert.AreEqual(1300, w.accumulatedVolume);
            }

            if (w.reportedVolumeFlag)
            {
                Assert.AreEqual(0, w.reportedVolume);
            }

            if (w.accumulatedTurnoverFlag)
            {
                Assert.AreEqual(10490, w.accumulatedTurnover);
            }

            if (w.lastTradeReportPriceFlag)
            {
                Assert.AreEqual(0, w.lastTradeReportPrice);
            }

            if (w.lastTradeReportQuantityFlag)
            {
                Assert.AreEqual(0, w.lastTradeReportQuantity);
            }

            if (w.orderbookFlushFlag)
            {
                Assert.AreEqual(true, w.orderbookFlush);
            }

            if (w.VWAPFlag)
            {
                Assert.AreEqual(8.069, w.VWAP);
            }

            if (w.VWAPDiffPerFlag)
            {
                Assert.AreEqual(0, w.VWAPDiffPer);
            }

            if (w.numberOfTradeReportsFlag)
            {
                Assert.AreEqual(0, w.numberOfTradeReports);
            }

            if (w.diffDayPerFlag)
            {
                Assert.AreEqual(0, w.diffDayPer);
            }

            if (w.TWAPFlag)
            {
                Assert.AreEqual(0, w.TWAP);
            }

            if (w.closingAuctionPriceFlag)
            {
                Assert.AreEqual(0, w.closingAuctionPrice);
            }

            if (w.lastVolumeFlag)
            {
                Assert.AreEqual(1000, w.lastVolume);
            }

            if (w.remainingQuantityFlag)
            {
                Assert.AreEqual(0, w.remainingQuantity);
            }
        }
    }
}