﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataSectorTest
    {
        [Test]
        public void generateBasicDataSector()
        {
            BasicDataSector BDs = new BasicDataSector
            (
                "BDs;i160;Si830;s1;NAmFINANSAL KIRALAMA VE FACTORING SIRKETLER;CDLv1;"
            );

            Assert.AreEqual(160, BDs.ID);
            Assert.AreEqual("830", BDs.sourceID);
            Assert.AreEqual(1, BDs.sourceSystem);

            if (BDs.symbolFlag == true)
                Assert.AreEqual(0, BDs.symbol);

            Assert.AreEqual("FINANSAL KIRALAMA VE FACTORING SIRKETLER", BDs.name);

            Assert.AreEqual(1, BDs.codeLevel);

            if (BDs.parentIDflag == true)
                Assert.AreEqual(0, BDs.parentID);
        }
    }
}