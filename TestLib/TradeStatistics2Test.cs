﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class TradeStatistics2Test
    {
        [Test]
        public void generateTradeStatistics2()
        {
            TradeStatistics2 v = new TradeStatistics2
            (
                "v;i1622;s1;t095223.000;Pf21;Pl18.92;Ph23;" +
                "LOp18.92;Pd-1.58;Wp20.976;Wd0;Dd-7.71;"
            );

            Assert.AreEqual(1622, v.ID);
            Assert.AreEqual(1, v.sourceSystem);
            Assert.AreEqual("09:52:23.000", v.timeExec);

            if (v.firstPriceFlag == true)
                Assert.AreEqual(21, v.firstPrice);

            if (v.lastPriceFlag == true)
                Assert.AreEqual(18.92, v.lastPrice);

            if (v.highPriceFlag == true)
                Assert.AreEqual(23, v.highPrice);

            if (v.lowPriceFlag == true)
                Assert.AreEqual(18.92, v.lowPrice);

            if (v.diffLastPriceFlag == true)
                Assert.AreEqual(-1.58, v.diffLastPrice);

            if (v.lastTradeReportPriceFlag == true)
                Assert.AreEqual(-1.58, v.lastTradeReportPrice);

            if (v.orderbookFlushFlag == true)
                Assert.AreEqual(true, v.orderbookFlush);

            if (v.VWAPFlag == true)
                Assert.AreEqual(20.976, v.VWAP);

            if (v.VWAPDiffPerFlag == true)
                Assert.AreEqual(0, v.VWAPDiffPer);

            if (v.diffDayPerFlag == true)//
                Assert.AreEqual(-7.71, v.diffDayPer);

            if (v.closingAuctionPriceFlag == true)
                Assert.AreEqual(0, v.closingAuctionPrice);
        }
    }
}