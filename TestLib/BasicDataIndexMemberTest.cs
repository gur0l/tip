﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataIndexMemberTest
    {
        [Test]
        public void generateBasicDataIndexMember()
        {
            BasicDataIndexMember BDIm = new BasicDataIndexMember
            (
                "BDIm;IDo388;IXi2548;s2;"
            );

            Assert.AreEqual(388, BDIm.orderBookID);
            Assert.AreEqual(2548, BDIm.indexID);
            Assert.AreEqual(2, BDIm.sourceSystem);
        }
    }
}