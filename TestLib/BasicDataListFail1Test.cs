﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataListFail1Test
    {
        [Test]
        public void generateBasicDataListFail1()
        {
            BasicDataList BDLi = new BasicDataList
            (
                "BDLi;i12;Si255;s1;NAmSYSTEM;LCyTRY;TCeY;"
            );

            Assert.AreEqual(12, BDLi.ID);
            Assert.AreEqual("255", BDLi.sourceID);
            Assert.AreEqual(1, BDLi.sourceSystem);

            if (BDLi.parentIDflag == true)
                Assert.AreEqual(8, BDLi.parentID);

            if (BDLi.symbolFlag == true)
                Assert.AreEqual("XXX", BDLi.symbol);

            Assert.AreEqual("SYSTEM", BDLi.name);
            Assert.AreEqual("TRY", BDLi.currency);
            Assert.AreEqual('Y', BDLi.TurnoverCalculationEnabled);
        }
    }
}