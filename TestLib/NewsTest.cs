﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class NewsTest
    {
        [Test]
        public void generateNews()
        {
            News n = new News
            (
                "n;s1;i598;NWi1;t140705.249;NOt3;NMsBORSA_BASKANLIGI_DUYURUSU;" +
                "NHlTest;TExCok yogun Islem yapildigi goruldu. " +
                "Sozkonusu siralar gecici durdurmaya alindi.;BLi1;BLlY;"
            );

            Assert.AreEqual(1, n.sourceSystem);
            Assert.AreEqual(598, n.ID);
            Assert.AreEqual(1, n.newsID);
            Assert.AreEqual("14:07:05.249", n.timeExec);
            Assert.AreEqual(3, n.newsObjectType);
            Assert.AreEqual("BORSA_BASKANLIGI_DUYURUSU", n.messageSource);

            if (n.URLflag)
            {
                Assert.AreEqual("www.komutanlogar.com", n.URL);
                //semicolon case is not tested ON PURPOSE!
                //I want to see what happens if an input comes like that :3
            }

            Assert.AreEqual("Test", n.headline);
            Assert.AreEqual("Cok yogun Islem yapildigi goruldu. " +
                "Sozkonusu siralar gecici durdurmaya alindi.", n.text);
            Assert.AreEqual(1, n.blockID);
            Assert.AreEqual('Y', n.lastBlock);
        }
    }
}