﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class IndexUpdateTest
    {
        [Test]
        public void generateIndexUpdate()
        {
            IndexUpdate i = new IndexUpdate
            (
                "i;i2618;s2;t140706.000;Vc94499.8005;o103850;" +
                "f1098120;OVa94268.6961;Dn231.1044;Dd0.25;"
            );

            Assert.AreEqual(2618, i.ID);
            Assert.AreEqual(2, i.sourceSystem);
            Assert.AreEqual("14:07:06.000", i.timeExec);
            Assert.AreEqual(94499.8005, i.currentValue);

            if (i.highValueFlag)
            {
                Assert.AreEqual(0, i.highValue);
            }

            if (i.lowValueFlag)
            {
                Assert.AreEqual(0, i.lowValue);
            }

            Assert.AreEqual(103850, i.accumulatedVolume);
            Assert.AreEqual(1098120, i.accumulatedTurnover);

            if (i.openValueFlag)
            {
                Assert.AreEqual(94268.6961, i.openValue);
            }

            Assert.AreEqual(231.1044, i.diffDayNom);
            Assert.AreEqual(0.25, i.diffDayPer);
        }
    }
}