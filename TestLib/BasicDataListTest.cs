﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataListTest
    {
        [Test]
        public void generateBasicDataList()
        {
            BasicDataList BDLi = new BasicDataList
            (
                "BDLi;i298;Si080130;s1;PAi8;SYmBUYIN;NAmPAY - TEMERRUT PAZARI (Buyin Mk);LCyTRY;TCeY;"
            );

            Assert.AreEqual(298, BDLi.ID);
            Assert.AreEqual("080130", BDLi.sourceID);
            Assert.AreEqual(1, BDLi.sourceSystem);

            if (BDLi.parentIDflag == true)
                Assert.AreEqual(8, BDLi.parentID);

            Assert.AreEqual("BUYIN", BDLi.symbol);
            Assert.AreEqual("PAY - TEMERRUT PAZARI (Buyin Mk)", BDLi.name);
            Assert.AreEqual("TRY", BDLi.currency);

            Assert.AreEqual('Y', BDLi.TurnoverCalculationEnabled);
        }
    }
}