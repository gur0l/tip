﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataTradingSchemeTest
    {
        [Test]
        public void generateBasicDataTradingScheme()
        {
            BasicDataTradingScheme BDTm = new BasicDataTradingScheme
            (
                "BDTm;s1;TRId102;SiP_BP_TEK_TRF;Dt20150730;Ms21;St103000.000;"
            );

            Assert.AreEqual(1, BDTm.sourceSystem);
            Assert.AreEqual(102, BDTm.tradingSession);
            Assert.AreEqual("P_BP_TEK_TRF", BDTm.sourceID);

            DateTime testDate = new DateTime(2015, 07, 30);

            Assert.AreEqual(testDate, BDTm.date);

            Assert.AreEqual(21, BDTm.stateCode);

            Assert.AreEqual("10:30:00.000", BDTm.startTime);
        }
    }
}