﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class Orderbook1Test
    {
        [Test]
        public void generateOrderbook1()
        {
            Orderbook1 o = new Orderbook1
            (
                "o;i950;s1;t093000.303;b1:8.99;a1:9.02;b2:8.5;c3;"
            );

            Assert.AreEqual(950, o.ID);
            Assert.AreEqual(1, o.sourceSystem);
            Assert.AreEqual("09:30:00.303", o.timeExec);

            if (o.orderbookFlushFlag)
            {
                Assert.AreEqual(true, o.orderbookFlush);
            }

            if (o.bidLevelDeletedFlag)
            {
                Assert.AreEqual(5, o.c[0]);
                Assert.AreEqual(9, o.c[1]);
            }

            if (o.askLevelDeletedFlag)
            {
                Assert.AreEqual(8, o.e[0]);
                Assert.AreEqual(99, o.e[1]);
            }

            if (o.bidPriceDiffFlag)
            {
                Assert.AreEqual(0, o.bidPriceDiff);
            }

            if (o.bidPriceAtLevelFlag)
            {
                Assert.AreEqual(8.99, o.b[0]);
                Assert.AreEqual(8.5, o.b[1]);
            }

            if (o.askPriceAtLevelFlag)
            {
                Assert.AreEqual(9.02, o.a[0]);
            }
        }
    }
}