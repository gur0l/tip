﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class StateChangeTest
    {
        [Test]
        public void generateStateChange()
        {
            StateChange s = new StateChange
            (
                "s;i1466;s1;t070203.014;Ms27;Sl2;"
            );

            Assert.AreEqual(1466, s.ID);
            Assert.AreEqual(1, s.sourceSystem);
            Assert.AreEqual("07:02:03.014", s.timeExec);
            Assert.AreEqual(27, s.stateCode);
            Assert.AreEqual(2, s.stateLevel);

            if (s.orderbookFlushFlag)
            {
                Assert.AreEqual(true, s.orderbookFlush);
            }
        }
    }
}