﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataNonTradingDaysTest
    {
        [Test]
        public void generateBasicDataMarket()
        {
            BasicDataNonTradingDays BDTd = new BasicDataNonTradingDays
            (
                "BDTd;s1;i300;Si080120;Dt20181029;TDt2162;"
            );

            Assert.AreEqual(1, BDTd.sourceSystem);
            Assert.AreEqual(300, BDTd.ID);
            Assert.AreEqual("080120", BDTd.sourceID);

            DateTime testDate = new DateTime(2018, 10, 29);

            Assert.AreEqual(testDate, BDTd.date);

            Assert.AreEqual(2162, BDTd.dayType);
        }
    }
}