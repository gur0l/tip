﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class IndexSummaryTest
    {
        [Test]
        public void generateIndexSummary()
        {
            IndexSummary IsTest = new IndexSummary
            (
                "Is;i2550;s2;t171601.000;CLv78526.5519;Vh78526.5519;" +
                "Vl78298.4986;o1103850;f9898120;OVa78322.7848;" +
                "IXDv1944808.183487;Dn203.767139;Dd0.260163;" +
                "CAp152719080848.566803;SEd2;"
            );

            Assert.AreEqual(2550, IsTest.ID);
            Assert.AreEqual(2, IsTest.sourceSystem);
            Assert.AreEqual("17:16:01.000", IsTest.timeExec);

            if (IsTest.closingValueFlag)
                Assert.AreEqual(78526.5519, IsTest.closingValue);

            if (IsTest.highValueFlag)
                Assert.AreEqual(78526.5519, IsTest.highValue);

            if (IsTest.lowValueFlag)
                Assert.AreEqual(78298.4986, IsTest.lowValue);

            Assert.AreEqual(1103850, IsTest.accumulatedVolume);
            Assert.AreEqual(9898120, IsTest.accumulatedTurnover);

            if (IsTest.openValueFlag)
                Assert.AreEqual(78322.7848, IsTest.openValue);

            if (IsTest.oldIndexValueFlag)
                Assert.AreEqual(78322.7848, IsTest.oldIndexValue);

            Assert.AreEqual(1944808.183487, IsTest.divisor);
            Assert.AreEqual(203.767139, IsTest.diffDayNom);
            Assert.AreEqual(0.260163, IsTest.diffDayPer);
            Assert.AreEqual(152719080848.566803, IsTest.marketCap);
            Assert.AreEqual(2, IsTest.sodEod);
        }
    }
}