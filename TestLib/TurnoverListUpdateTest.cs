﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class TurnoverListUpdateTest
    {
        [Test]
        public void generateTurnoverListUpdate()
        {
            TurnoverListUpdate l = new TurnoverListUpdate
            (
                "l;i8;s1;t093011.876;f2170975;o146350;Bu885;Pu881;Pp3;Pm1;TNt11;"
            );

            Assert.AreEqual(8, l.ID);
            Assert.AreEqual(1, l.sourceSystem);
            Assert.AreEqual("09:30:11.876", l.timeExec);
            Assert.AreEqual(2170975, l.accumulatedTurnover);
            Assert.AreEqual(146350, l.accumulatedVolume);
            Assert.AreEqual(885, l.unchangedBids);

            if (l.plusBidsFlag)
                Assert.AreEqual(881, l.plusBids);

            if (l.minusBidsFlag)
                Assert.AreEqual(1, l.minusBids);

            Assert.AreEqual(881, l.unchangedPaid);

            if (l.plusPaidFlag)
                Assert.AreEqual(3, l.plusPaid);

            if (l.minusPaidFlag)
                Assert.AreEqual(1, l.minusPaid);

            Assert.AreEqual(11, l.totalNumberOfTrades);
        }
    }
}