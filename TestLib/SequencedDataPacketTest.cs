﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;
/*
@Test
public void parseSequencedDataPacketTest()
{
byte[] message = new byte[] { 65, 66, 67 };
SequencedDataPacket packet = new SequencedDataPacket(message);
assertArrayEquals(packet.buf, new byte[]{0, 4,  //packet len
                                   83,    //packet type
                                   65, 66, 67}); //message
*/
namespace TestLib
{
    [TestFixture]
    public class SequencedDataPacketTest
    {
        [Test]
        public void generateSequencedDataPacket()
        {
            byte[] message = new byte[] { 65, 66, 67 };
            string toString = Encoding.ASCII.GetString(message);

            SequencedDataPacket packet = new SequencedDataPacket(toString);
            
            Assert.AreEqual(packet.readyPacket, new byte[]{0, 4,
                                                           83,    
                                                           65, 66, 67});
        }
    }
}
