﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class MarketMakerQuote1Test
    {
        [Test]
        public void generateMarketMakerQuote1()
        {
            MarketMakerQuote1 q = new MarketMakerQuote1
            (
                "q;i1706;s1;t141023.154;Pa2.77;Pb2.7;"
            );

            Assert.AreEqual(1706, q.ID);
            Assert.AreEqual(1, q.sourceSystem);
            Assert.AreEqual("14:10:23.154", q.timeExec);

            if (q.askPriceFlag)
                Assert.AreEqual(2.77, q.askPrice);

            if (q.bidPriceFlag)
                Assert.AreEqual(2.7, q.bidPrice);
        }
    }
}