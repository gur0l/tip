﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataBusinessDateTest
    {
        [Test]
        public void generateBasicDataBusinessDate()
        {
            BasicDataBusinessDate BDBu = new BasicDataBusinessDate("BDBu;Dt20150730;");

            DateTime testDate = new DateTime(2015,07,30);

            Assert.AreEqual(testDate, BDBu.date);
        }
    }
}
