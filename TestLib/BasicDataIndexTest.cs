﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataIndexTest
    {
        [Test]
        public void generateBasicDataIndex()
        {
            BasicDataIndex BDIn = new BasicDataIndex
            (
                "BDIn;i2548;SiXUTUM;s2;SYmXUTUM;NAmBIST TUM TRY;LCyTRY;Is2512;Di1;HOt;"
            );

            Assert.AreEqual(2548, BDIn.ID);
            Assert.AreEqual("XUTUM", BDIn.sourceID);
            Assert.AreEqual(2, BDIn.sourceSystem);
            Assert.AreEqual("XUTUM", BDIn.symbol);
            Assert.AreEqual("BIST TUM TRY", BDIn.name);
            Assert.AreEqual("TRY", BDIn.currency);

            if (BDIn.populationTypeFlag)
                Assert.AreEqual(2542, BDIn.populationType);

            if (BDIn.calculationTypeFlag)
                Assert.AreEqual(2538, BDIn.calculationType);

            if (BDIn.indexTypeFlag)
                Assert.AreEqual(2514, BDIn.indexType);

            if (BDIn.indexPriceTypeFlag)
                Assert.AreEqual(2528, BDIn.indexPriceType);

            Assert.AreEqual(2512, BDIn.indexStatus);

            if (BDIn.indexOwnerFlag)
                Assert.AreEqual(2544, BDIn.indexOwner);

            Assert.AreEqual(1, BDIn.disseminationInterval);

            if (BDIn.hotInsertedFlag)
            {
                Assert.AreEqual(true, BDIn.hotInsertedFlag);
            }

            if (BDIn.sectorIDFlag)
            {
                Assert.AreEqual(0, BDIn.sectorID);
            }
        }
    }
}