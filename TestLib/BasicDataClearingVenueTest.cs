﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataClearingVenueTest
    {
        [Test]
        public void generateBasicDataClearingValue()
        {
            BasicDataClearingVenue BDCv = new BasicDataClearingVenue
            (
                "BDCv;i250;SiTVS;s1;SYmTVSBTRISXXX;NAmTAKASBANK;"
            );

            Assert.AreEqual(250, BDCv.ID);
            Assert.AreEqual("TVS", BDCv.sourceID);
            Assert.AreEqual(1, BDCv.sourceSystem);
            Assert.AreEqual("TVSBTRISXXX", BDCv.symbol);
            Assert.AreEqual("TAKASBANK", BDCv.name);
        }
    }
}