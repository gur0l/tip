﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class MarketMakerQuote2Test
    {
        [Test]
        public void generateMarketMakerQuote2()
        {
            MarketMakerQuote2 y = new MarketMakerQuote2
            (
                "y;i1706;s1;t141023.154;Pa2.77;Pb2.7;Vb1000;Va750;"
            );

            Assert.AreEqual(1706, y.ID);
            Assert.AreEqual(1, y.sourceSystem);
            Assert.AreEqual("14:10:23.154", y.timeExec);

            if (y.askPriceFlag)
                Assert.AreEqual(2.77, y.askPrice);

            if (y.bidPriceFlag)
                Assert.AreEqual(2.7, y.bidPrice);

            if (y.bidVolumeFlag)
                Assert.AreEqual(1000, y.bidVolume);

            if (y.askVolumeFlag)
                Assert.AreEqual(750, y.askVolume);
        }
    }
}