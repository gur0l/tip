﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataTickSizeEntryTest
    {
        [Test]
        public void generateBasicDataTickSizeEntry()
        {
            BasicDataTickSizeEntry BDTz = new BasicDataTickSizeEntry
            (
                "BDTz;i68;SiEQ_WR;s1;TSz0.001;PFr0.001;PTo999999.999;"
            );

            Assert.AreEqual(68, BDTz.ID);
            Assert.AreEqual("EQ_WR", BDTz.sourceID);
            Assert.AreEqual(1, BDTz.sourceSystem);
            Assert.AreEqual(0.001, BDTz.tickSize);
            Assert.AreEqual(0.001, BDTz.priceFrom);
            Assert.AreEqual(999999.999, BDTz.priceTo);
        }
    }
}