﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class Orderbook2Test
    {
        [Test]
        public void generateOrderbook2()
        {
            Orderbook2 p = new Orderbook2
            (
                "p;i1278;s1;t152423.148;a1:4.69;j1:600000;k1:1;a2:4.7;" +
                "j2:600000;k2:1;a3:4.71;j3:600000;k3:1;a4:4.72;j4:600000;" +
                "k4:1;a5:4.73;j5:600000;k5:1;a6:4.74;j6:600000;k6:1;a7:4.75;" +
                "j7:600000;k7:1;a8:4.76;j8:600000;k8:1;a9:4.77;j9:600000;" +
                "k9:1;a10:4.78;j10:600000;k10:1;"
            );

            Assert.AreEqual(1278, p.ID);
            Assert.AreEqual(1, p.sourceSystem);
            Assert.AreEqual("15:24:23.148", p.timeExec);

            if (p.orderbookFlushFlag)
            {
                Assert.AreEqual(true, p.orderbookFlush);
            }

            if (p.bidLevelDeletedFlag)
            {
                Assert.AreEqual(5, p.c[0]);
                Assert.AreEqual(9, p.c[1]);
            }

            if (p.askLevelDeletedFlag)
            {
                Assert.AreEqual(8, p.e[0]);
                Assert.AreEqual(99, p.e[1]);
            }

            if (p.bidPriceDiffFlag)
            {
                Assert.AreEqual(0, p.bidPriceDiff);
            }

            if (p.bidPriceAtLevelFlag)
            {
                Assert.AreEqual(1, p.b[0]);
                Assert.AreEqual(7, p.b[1]);
            }

            if (p.bidVolumeAtLevelFlag)
            {
                Assert.AreEqual(1, p.g[0]);
                Assert.AreEqual(7, p.g[1]);
            }

            if (p.bidOrdersAtLevelFlag)
            {
                Assert.AreEqual(1, p.h[0]);
                Assert.AreEqual(7, p.h[1]);
            }

            if (p.askPriceAtLevelFlag)
            {
                Assert.AreEqual(4.69, p.a[0]);
                Assert.AreEqual(4.7, p.a[1]);
                Assert.AreEqual(4.71, p.a[2]);
                Assert.AreEqual(4.72, p.a[3]);
                Assert.AreEqual(4.73, p.a[4]);
                Assert.AreEqual(4.74, p.a[5]);
                Assert.AreEqual(4.75, p.a[6]);
                Assert.AreEqual(4.76, p.a[7]);
                Assert.AreEqual(4.77, p.a[8]);
                Assert.AreEqual(4.78, p.a[9]);
            }

            if (p.askVolumeAtLevelFlag)
            {
                Assert.AreEqual(600000, p.j[0]);
                Assert.AreEqual(600000, p.j[1]);
                Assert.AreEqual(600000, p.j[2]);
                Assert.AreEqual(600000, p.j[3]);
                Assert.AreEqual(600000, p.j[4]);
                Assert.AreEqual(600000, p.j[5]);
                Assert.AreEqual(600000, p.j[6]);
                Assert.AreEqual(600000, p.j[7]);
                Assert.AreEqual(600000, p.j[8]);
                Assert.AreEqual(600000, p.j[10]);
            }

            if (p.askOrdersAtLevelFlag)
            {
                Assert.AreEqual(1, p.k[0]);
                Assert.AreEqual(1, p.k[1]);
                Assert.AreEqual(1, p.k[2]);
                Assert.AreEqual(1, p.k[3]);
                Assert.AreEqual(1, p.k[4]);
                Assert.AreEqual(1, p.k[5]);
                Assert.AreEqual(1, p.k[6]);
                Assert.AreEqual(1, p.k[7]);
                Assert.AreEqual(1, p.k[8]);
                Assert.AreEqual(1, p.k[9]);
            }
        }
    }
}