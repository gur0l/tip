﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class TradeTest
    {
        [Test]
        public void generateTrade()
        {
            Trade t = new Trade
            (
                "t;i950;s1;Tn1;t093000.409;TDi093000.409;p9.02;v10000;UlY;UhY;UvY;LtY;TiCSTWgQAAAAEAAAAA;"
            );

            DateTime testDate = new DateTime(0001, 01, 01);

            Assert.AreEqual(950, t.ID);
            Assert.AreEqual(1, t.sourceSystem);
            Assert.AreEqual(1, t.tradeNumber);

            if(t.tradeCancelFlag)
                Assert.AreEqual('Y', t.tradeCancel);

            if (t.timeExecFlag)
                Assert.AreEqual("09:30:00.409", t.timeExec);

            if (t.timestampAgreementFlag)
                Assert.AreEqual("00:00:00.000", t.timestampAgreement);

            if (t.timestampTradeCancelFlag)
                Assert.AreEqual("11:49:17.465", t.timestampTradeCancel);

            if (t.timestampDisseminationFlag)
                Assert.AreEqual("09:30:00.409", t.timestampDissemination);

            if (t.settlementDateFlag)
                Assert.AreEqual(testDate, t.settlementDate);

            Assert.AreEqual(9.02, t.price);
            Assert.AreEqual(10000, t.volume);

            if (t.tradeBuyerFlag)
                Assert.AreEqual("", t.tradeBuyer);

            if (t.tradeSellerFlag)
                Assert.AreEqual("", t.tradeSeller);

            if (t.tradeTypeFlag)
                Assert.AreEqual(0, t.tradeType);

            if (t.tradeClassFlag)
                Assert.AreEqual(0, t.tradeClass);

            Assert.AreEqual('Y', t.tradeUpdatesLastPaid);
            Assert.AreEqual('Y', t.tradeUpdatesHighLow);
            Assert.AreEqual('Y', t.tradeUpdatesTurnover);
            Assert.AreEqual('Y', t.latestTrade);

            if (t.dateExecFlag)
                Assert.AreEqual(testDate, t.dateExec);

            if (t.dateAgreementFlag)
                Assert.AreEqual(testDate, t.dateAgreement);

            if (t.dateDisseminationFlag)
                Assert.AreEqual(testDate, t.dateDissemination);

            if (t.dateTradeCancelFlag)
                Assert.AreEqual(testDate, t.dateTradeCancel);

            Assert.AreEqual("CSTWgQAAAAEAAAAA", t.tradeID);

            if (t.aggressivePartyFlag)
                Assert.AreEqual('S', t.aggressiveParty);
        }
    }
}