﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataExchangeTest
    {
        [Test]
        public void generateBasicDataExchange()
        {
            BasicDataExchange BDx = new BasicDataExchange
            (
                "BDx;i6;Si080;s1;SYmXIST;NAmBorsa Istanbul;CNyTR;MIcXIST;"
            );

            Assert.AreEqual(6, BDx.ID);
            Assert.AreEqual("080", BDx.sourceID);
            Assert.AreEqual(1, BDx.sourceSystem);
            Assert.AreEqual("XIST", BDx.symbol);
            Assert.AreEqual("Borsa Istanbul", BDx.name);
            Assert.AreEqual("TR", BDx.country);

            if (BDx.micFlag == true)
                Assert.AreEqual("XIST", BDx.micCode);
        }
    }
}