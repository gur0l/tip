﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class OrderbookReferencePriceTest
    {
        [Test]
        public void generateOrderbookReferencePrice()
        {
            OrderbookReferencePrice r = new OrderbookReferencePrice
            (
                "r;s1;i838;t144905.232;RPr4.21;"
            );

            Assert.AreEqual(1, r.sourceSystem);
            Assert.AreEqual(838, r.ID);
            Assert.AreEqual("14:49:05.232", r.timeExec);
            Assert.AreEqual(4.21, r.basePrice);

            if (r.additionalReferencePriceFlag)
                Assert.AreEqual(0, r.additionalReferencePrice);

            if (r.lowerPriceLimitFlag)
                Assert.AreEqual(11.44, r.lowerPriceLimit);

            if (r.upperPriceLimitFlag)
                Assert.AreEqual(17.16, r.upperPriceLimit);
        }
    }
}