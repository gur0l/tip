﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class TradeStatistics1Test
    {
        [Test]
        public void generateTradeStatistics1()
        {
            TradeStatistics1 u = new TradeStatistics1
            (
                "u;i1622;s1;t095223.000;Pl18.92;Pd-1.58;Dd-7.71;"
            );

            Assert.AreEqual(1622, u.ID);
            Assert.AreEqual(1, u.sourceSystem);
            Assert.AreEqual("09:52:23.000", u.timeExec);

            if (u.lastPriceFlag == true)
                Assert.AreEqual(18.92, u.lastPrice);

            if (u.diffLastPriceFlag == true)
                Assert.AreEqual(-1.58, u.diffLastPrice);

            if (u.orderbookFlushFlag == true)
                Assert.AreEqual(true, u.orderbookFlush);

            if (u.diffDayPerFlag == true)
                Assert.AreEqual(-7.71, u.diffDayPer);
        }
    }
}