﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class IndexWeightTest
    {
        [Test]
        public void generateIndexWeight()
        {
            IndexWeight Iw = new IndexWeight
            (
                "Iw;i2550;s2;t041000.000;IDo1736;WPe2.627826;FFr0.35;" +
                "Wf1;WFv4002778625.552;CAp11436510358.719999;SEd1;"
            );

            Assert.AreEqual(2550, Iw.ID);
            Assert.AreEqual(2, Iw.sourceSystem);
            Assert.AreEqual("04:10:00.000", Iw.timeExec);
            Assert.AreEqual(1736, Iw.orderBookID);
            Assert.AreEqual(2.627826, Iw.weightPercent);
            Assert.AreEqual(0.35, Iw.freeFloatRatio);
            Assert.AreEqual(1, Iw.weightingFactor);
            Assert.AreEqual(4002778625.552, Iw.weightedFreeFloatMktValue);
            Assert.AreEqual(11436510358.719999, Iw.marketCap);
            Assert.AreEqual(1, Iw.sodEod);
        }
    }
}