﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataIssuerTest
    {
        [Test]
        public void generateBasicDataIssuer()
        {
            BasicDataIssuer BDIs = new BasicDataIssuer
            (
                "BDIs;i372;SiGRM;s1;SYmGRM;NAmGARANTI MENKUL KIYMETLER A.S.;CNyTR;"
            );

            Assert.AreEqual(372, BDIs.ID);
            Assert.AreEqual("GRM", BDIs.sourceID);
            Assert.AreEqual(1, BDIs.sourceSystem);

            Assert.AreEqual("GRM", BDIs.symbol);
            Assert.AreEqual("GARANTI MENKUL KIYMETLER A.S.", BDIs.name);
            Assert.AreEqual("TR", BDIs.country);
        }
    }
}