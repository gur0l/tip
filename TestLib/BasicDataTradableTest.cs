﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataTradableTest
    {
        [Test]
        public void generateBasicDataTradable()
        {
            BasicDataTradable BDt = new BasicDataTradable
            (
                "BDt;i486;SiBURCE.R;s1;Mk288;INiMSPOTRGTBURCER;" +
                "SYmBURCE.R;NAmBURCELIK RIGHT COUPONS;ISi368;CUiTRY;" +
                "CUtTRY;PRt1;VOd1;LDa20141113;ITSz78;NDp3;NDTp3;STy2;" +
                "AUmY;TRaY;PTaY;PTb1;LSz1;MLm1;MLt10000000;TRId112;GRsN;"
            );

            Assert.AreEqual(486, BDt.ID);
            Assert.AreEqual("BURCE.R", BDt.sourceID);
            Assert.AreEqual(1, BDt.sourceSystem);
            Assert.AreEqual(288, BDt.marketID);
            Assert.AreEqual("MSPOTRGTBURCER", BDt.instrumentSourceID);
            Assert.AreEqual("BURCE.R", BDt.symbol);
            Assert.AreEqual("BURCELIK RIGHT COUPONS", BDt.name);

            if (BDt.abbreviatedNameFlag)
            {
                Assert.AreEqual("XXX", BDt.abbreviatedName);
            }

            Assert.AreEqual(368, BDt.issuerID);
            Assert.AreEqual("TRY", BDt.issueCurrency);
            Assert.AreEqual("TRY", BDt.tradingCurrency);
            Assert.AreEqual(1, BDt.priceType);
            Assert.AreEqual(1, BDt.volumeDimention);

            DateTime listingDateTest = new DateTime(2014, 11, 13);

            Assert.AreEqual(listingDateTest, BDt.listingDate);

            if (BDt.tradedThroughDateFlag)
            {
                DateTime tradedThroughDateTest = new DateTime(0001, 01, 01);
                Assert.AreEqual(tradedThroughDateTest, BDt.tradedThroughDate);
            }

            if (BDt.instrumentExternalTextFlag)
            {
                Assert.AreEqual("Ericsson B", BDt.instrumentExternalText);
            }

            if (BDt.nominalValueFlag)
            {
                Assert.AreEqual("0.00001", BDt.nominalValue);
            }

            Assert.AreEqual(78, BDt.tickSizeTableID);
            Assert.AreEqual(3, BDt.noOfDecPrice);
            Assert.AreEqual(3, BDt.noOfDecTradePrice);

            if (BDt.clearingVenueIDFlag)
            {
                Assert.AreEqual(0, BDt.clearingVenueID);
            }

            if (BDt.countryFlag)
            {
                Assert.AreEqual("TR", BDt.country);
            }

            if (BDt.noOfSettlementDaysFlag)
            {
                Assert.AreEqual(0, BDt.noOfSettlementDays);
            }

            Assert.AreEqual(2, BDt.securityType);
            Assert.AreEqual('Y', BDt.automatch);
            Assert.AreEqual('Y', BDt.tradeReportsAllowed);
            Assert.AreEqual('Y', BDt.preTradeAnonimity);
            Assert.AreEqual(1, BDt.postTradeAnonimity);

            if (BDt.marketSegmentFlag)
            {
                Assert.AreEqual(40, BDt.marketSegment);
            }

            if (BDt.hotInsertedFlag)
            {
                Assert.AreEqual(true, BDt.hotInserted);
            }

            Assert.AreEqual(1, BDt.lotSize);

            if (BDt.shortSellValidationFlag)
            {
                Assert.AreEqual(1, BDt.shortSellValidation);
            }

            if (BDt.minimumLotFlag)
            {
                Assert.AreEqual(1, BDt.minimumLot);
            }

            if (BDt.maximumLotFlag)
            {
                Assert.AreEqual(10000000, BDt.maximumLot);
            }

            Assert.AreEqual(112, BDt.tradingSession);

            int count = 0;
            int[] values = { 2294 };

            if (BDt.marketMakerFlag)
            {
                foreach (var c in values)
                {
                    Assert.AreEqual(values[count], BDt.marketMaker[count]);
                }
            }

            if (BDt.tradingMethodFlag)
            {
                Assert.AreEqual("SM", BDt.tradingMethod);
            }

            Assert.AreEqual('N', BDt.grossSettlement);
        }
        public void generateBasicDataTradable2()
        {
            BasicDataTradable BDt = new BasicDataTradable
            (
                @"BDt;i396;SiGARIYMEPW999.MV;s1;Mk300;" +
                @"INiOFAUCEPWGARIYM;SYmGARIYMEPW999.MV;" +
                @"NAmGARANP3006140002.20IYM001\:002NA;ISi348;" +
                @"CUiTRY;CUtTRY;PRt1;VOd1;LDa20141023;ITSz68;" +
                @"NDp3;NDTp3;CNyTR;STy4;AUmY;TRaY;PTaY;PTb1;" +
                @"LSz10;MLm10;MLt10000000;TRId112;GRsN;"
            );

            Assert.AreEqual(396, BDt.ID);
            Assert.AreEqual("GARIYMEPW999.MV", BDt.sourceID);
            Assert.AreEqual(1, BDt.sourceSystem);
            Assert.AreEqual(300, BDt.marketID);
            Assert.AreEqual("OFAUCEPWGARIYM", BDt.instrumentSourceID);
            Assert.AreEqual("GARIYMEPW999.MV", BDt.symbol);
            Assert.AreEqual(@"GARANP3006140002.20IYM001\:002NA", BDt.name);

            if (BDt.abbreviatedNameFlag)
            {
                Assert.AreEqual("XXX", BDt.abbreviatedName);
            }

            Assert.AreEqual(348, BDt.issuerID);
            Assert.AreEqual("TRY", BDt.issueCurrency);
            Assert.AreEqual("TRY", BDt.tradingCurrency);
            Assert.AreEqual(1, BDt.priceType);
            Assert.AreEqual(1, BDt.volumeDimention);

            DateTime listingDateTest = new DateTime(2014, 10, 23);

            Assert.AreEqual(listingDateTest, BDt.listingDate);

            if (BDt.tradedThroughDateFlag)
            {
                DateTime tradedThroughDateTest = new DateTime(0001, 01, 01);
                Assert.AreEqual(tradedThroughDateTest, BDt.tradedThroughDate);
            }

            if (BDt.instrumentExternalTextFlag)
            {
                Assert.AreEqual("Ericsson B", BDt.instrumentExternalText);
            }

            if (BDt.nominalValueFlag)
            {
                Assert.AreEqual("0.00001", BDt.nominalValue);
            }

            Assert.AreEqual(68, BDt.tickSizeTableID);
            Assert.AreEqual(3, BDt.noOfDecPrice);
            Assert.AreEqual(3, BDt.noOfDecTradePrice);

            if (BDt.clearingVenueIDFlag)
            {
                Assert.AreEqual(0, BDt.clearingVenueID);
            }

            Assert.AreEqual("TR", BDt.country);

            if (BDt.noOfSettlementDaysFlag)
            {
                Assert.AreEqual(0, BDt.noOfSettlementDays);
            }

            Assert.AreEqual(4, BDt.securityType);
            Assert.AreEqual('Y', BDt.automatch);
            Assert.AreEqual('Y', BDt.tradeReportsAllowed);
            Assert.AreEqual('Y', BDt.preTradeAnonimity);
            Assert.AreEqual(1, BDt.postTradeAnonimity);

            if (BDt.marketSegmentFlag)
            {
                Assert.AreEqual(40, BDt.marketSegment);
            }

            if (BDt.hotInsertedFlag)
            {
                Assert.AreEqual(true, BDt.hotInserted);
            }

            Assert.AreEqual(10, BDt.lotSize);

            if (BDt.shortSellValidationFlag)
            {
                Assert.AreEqual(10, BDt.shortSellValidation);
            }

            if (BDt.minimumLotFlag)
            {
                Assert.AreEqual(1, BDt.minimumLot);
            }

            if (BDt.maximumLotFlag)
            {
                Assert.AreEqual(10000000, BDt.maximumLot);
            }

            Assert.AreEqual(112, BDt.tradingSession);

            int count = 0;
            int[] values = { 2294 };

            if (BDt.marketMakerFlag)
            {
                foreach (var c in values)
                {
                    Assert.AreEqual(values[count], BDt.marketMaker[count]);
                }
            }

            if (BDt.tradingMethodFlag)
            {
                Assert.AreEqual("SM", BDt.tradingMethod);
            }

            Assert.AreEqual('N', BDt.grossSettlement);
        }
    }
}