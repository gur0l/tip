﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class Orderbook3Test
    {
        [Test]
        public void generateOrderbook3()
        {
            Orderbook3 z = new Orderbook3
            (
                "z;i1672;s1;t173502.393;d-0.9;Bw39.056;" +
                "Bt28500;Aw40.005;At164500;b1:39.1;g1:16000;" +
                "h1:1;a1:40;j1:156000;k1:3;"
            );

            Assert.AreEqual(1672, z.ID);
            Assert.AreEqual(1, z.sourceSystem);
            Assert.AreEqual("17:35:02.393", z.timeExec);

            //double des = 0;

            if (z.wAvgPriceAllBidFlag)
            {
                Assert.AreEqual(39.056, z.wAvgPriceAllBid);
            }

            if (z.totVolAllBidFlag)
            {
                Assert.AreEqual(28500, z.totVolAllBid);
            }

            if (z.wAvgPriceAllAskFlag)
            {
                Assert.AreEqual(40.005, z.wAvgPriceAllAsk);
            }

            if (z.totVolAllAskFlag)
            {
                Assert.AreEqual(164500, z.totVolAllAsk);
            }

            if (z.orderbookFlushFlag)
            {
                Assert.AreEqual(true, z.orderbookFlush);
            }

            if (z.bidLevelDeletedFlag)
            {
                Assert.AreEqual(5, z.c[0]);
                Assert.AreEqual(9, z.c[1]);
            }

            if (z.askLevelDeletedFlag)
            {
                Assert.AreEqual(8, z.e[0]);
                Assert.AreEqual(99, z.e[1]);
            }

            if (z.bidPriceDiffFlag)
            {
                Assert.AreEqual(-0.9, z.bidPriceDiff);
            }

            if (z.bidPriceAtLevelFlag)
            {
                Assert.AreEqual(39.1, z.b[0]);
                /*Assert.AreEqual(3.42, z.b[1]);
                Assert.AreEqual(3.41, z.b[2]);
                Assert.AreEqual(3.4, z.b[3]);
                Assert.AreEqual(3.39, z.b[4]);
                Assert.AreEqual(3.38, z.b[5]);
                Assert.AreEqual(3.37, z.b[6]);
                Assert.AreEqual(3.36, z.b[7]);
                Assert.AreEqual(3.35, z.b[8]);
                Assert.AreEqual(3.34, z.b[9]);
                Assert.AreEqual(3.33, z.b[10]);
                Assert.AreEqual(3.32, z.b[11]);
                Assert.AreEqual(3.31, z.b[12]);
                Assert.AreEqual(3.3, z.b[13]);
                Assert.AreEqual(3.29, z.b[14]);
                Assert.AreEqual(3.28, z.b[15]);
                Assert.AreEqual(3.27, z.b[16]);
                Assert.AreEqual(3.26, z.b[17]);
                Assert.AreEqual(3.25, z.b[18]);
                Assert.AreEqual(3.24, z.b[19]);
                Assert.AreEqual(3.23, z.b[20]);
                Assert.AreEqual(3.22, z.b[21]);*/
            }

            if (z.bidVolumeAtLevelFlag)
            {
                //for(int i = 0; i < 22; i++)
                    Assert.AreEqual(16000, z.g[/*i*/1]);

            }

            if (z.bidOrdersAtLevelFlag)
            {
                //for (int i = 0; i < 22; i++)
                    Assert.AreEqual(1, z.h[/*i*/1]);
            }

            if (z.askPriceAtLevelFlag)
            {
                Assert.AreEqual(4.69, z.a[0]);
                Assert.AreEqual(4.7, z.a[1]);
                Assert.AreEqual(4.71, z.a[2]);
                Assert.AreEqual(4.72, z.a[3]);
                Assert.AreEqual(4.73, z.a[4]);
                Assert.AreEqual(4.74, z.a[5]);
                Assert.AreEqual(4.75, z.a[6]);
                Assert.AreEqual(4.76, z.a[7]);
                Assert.AreEqual(4.77, z.a[8]);
                Assert.AreEqual(4.78, z.a[9]);
            }

            if (z.askVolumeAtLevelFlag)
            {
                Assert.AreEqual(600000, z.j[0]);
                Assert.AreEqual(600000, z.j[1]);
                Assert.AreEqual(600000, z.j[2]);
                Assert.AreEqual(600000, z.j[3]);
                Assert.AreEqual(600000, z.j[4]);
                Assert.AreEqual(600000, z.j[5]);
                Assert.AreEqual(600000, z.j[6]);
                Assert.AreEqual(600000, z.j[7]);
                Assert.AreEqual(600000, z.j[8]);
                Assert.AreEqual(600000, z.j[10]);
            }

            if (z.askOrdersAtLevelFlag)
            {
                Assert.AreEqual(1, z.k[0]);
                Assert.AreEqual(1, z.k[1]);
                Assert.AreEqual(1, z.k[2]);
                Assert.AreEqual(1, z.k[3]);
                Assert.AreEqual(1, z.k[4]);
                Assert.AreEqual(1, z.k[5]);
                Assert.AreEqual(1, z.k[6]);
                Assert.AreEqual(1, z.k[7]);
                Assert.AreEqual(1, z.k[8]);
                Assert.AreEqual(1, z.k[9]);
            }
        }
    }
}