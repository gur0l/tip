﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataShareTest
    {
        [Test]
        public void generateBasicDataShare()
        {
            BasicDataShare BDSh = new BasicDataShare
            (
                "BDSh;i844;SiYKBNK.HE;s1;AVq50000;As20141112;Ae20141114;ICl30;"
            );

            Assert.AreEqual(844, BDSh.ID);
            Assert.AreEqual("YKBNK.HE", BDSh.sourceID);
            Assert.AreEqual(1, BDSh.sourceSystem);

            if (BDSh.hotInsertedFlag)
                Assert.AreEqual(true, BDSh.hotInserted);

            if (BDSh.availableQuantityAtStartFlag)
                Assert.AreEqual(50000, BDSh.availableQuantityAtStart);

            DateTime testDateAs = new DateTime(2014, 11, 12);
            DateTime testDateAe = new DateTime(2014, 11, 14);

            if (BDSh.availableQtyStartDateFlag)
                Assert.AreEqual(testDateAs, BDSh.availableQtyStartDate);

            if (BDSh.availableQtyEndDateFlag)
                Assert.AreEqual(testDateAe, BDSh.availableQtyEndDate);

            if (BDSh.instrumentClassificationFlag)
                Assert.AreEqual(30, BDSh.instrumentClassification);

            if (BDSh.totalIssueFlag)
                Assert.AreEqual(0, BDSh.totalIssue);
        }
    }
}