﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataSectorMemberTest
    {
        [Test]
        public void generateBasicDataSectorMember()
        {
            BasicDataSectorMember BDSm = new BasicDataSectorMember
            (
                "BDSm;IDo2132;SId130;s1;"
            );

            Assert.AreEqual(2132, BDSm.orderbookID);
            Assert.AreEqual(130, BDSm.sectorID);
            Assert.AreEqual(1, BDSm.sourceSystem);
        }
    }
}