﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class BasicDataMarketTest
    {
        [Test]
        public void generateBasicDataMarket()
        {
            BasicDataMarket BDm = new BasicDataMarket
            (
                "BDm;i252;Si255254;s1;Ex10;NAmINTEREST RATE MARKET;SYmINTRT;TOTa+0300;LDa20140826;"
            );

            Assert.AreEqual(252, BDm.ID);
            Assert.AreEqual("255254", BDm.sourceID);
            Assert.AreEqual(1, BDm.sourceSystem);
            Assert.AreEqual(10, BDm.exchangeID);
            Assert.AreEqual("INTEREST RATE MARKET", BDm.name);
            Assert.AreEqual("INTRT", BDm.symbol);
            Assert.AreEqual("+0300", BDm.timeOffsetUTC);

            DateTime testDate = new DateTime(2014, 08, 26);

            Assert.AreEqual(testDate, BDm.listingDate);

            if (BDm.tradedThroughDateFlag == true)
                Assert.AreEqual(testDate, BDm.tradedThroughDate); 
        }
    }
}