﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TestLib
{
    [TestFixture]
    public class CorporateActionTest
    {
        [Test]
        public void generateCorporateAction()
        {
            CorporateAction TRh = new CorporateAction
            (
                "TRh;i1934;s1;SRe1;NOc18;t070202.937;"
            );

            Assert.AreEqual(1934, TRh.ID);
            Assert.AreEqual(1, TRh.sourceSystem);
            Assert.AreEqual(1, TRh.actionStatus);
            Assert.AreEqual(18, TRh.noteCode);
            Assert.AreEqual("07:02:02.937", TRh.timeExec);
        }
    }
}