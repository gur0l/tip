﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class IndexWeight : TipMsg
    {
        public readonly int ID;
        public readonly int sourceSystem;
        public readonly String timeExec;
        public readonly int orderBookID;
        public readonly double weightPercent;
        public readonly double freeFloatRatio;
        public readonly double weightingFactor;
        public readonly double weightedFreeFloatMktValue;
        public readonly double marketCap;
        public readonly int sodEod;

        public IndexWeight(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceSystem = result[2];
            string firstTimeExec = result[3];
            string firstOrderBookID = result[4];
            string firstWeightPercent = result[5];
            string firstFreeFloatRatio = result[6];
            string firstWeightingFactor = result[7];
            string firstWeightedFreeFloatMktValue = result[8];
            string firstMarketCap = result[9];
            string firstSodEod = result[10];

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            timeExec = firstTimeExec.TrimStart('t');

            var builder = new StringBuilder();
            int count = 0;
            foreach (var c in timeExec)
            {
                builder.Append(c);

                if (!(count >= 4))
                {
                    if ((++count % 2) == 0)
                    {
                        builder.Append(':');
                    }
                }
            }
            timeExec = builder.ToString();

            firstOrderBookID = firstOrderBookID.Substring
                    (3, firstOrderBookID.Length - 3);
            orderBookID = Int32.Parse(firstOrderBookID);

            firstWeightPercent = firstWeightPercent.Substring
                    (3, firstWeightPercent.Length - 3);

            weightPercent = double.Parse
            (
                firstWeightPercent, CultureInfo.InvariantCulture.NumberFormat
            );

            firstFreeFloatRatio = firstFreeFloatRatio.Substring
                    (3, firstFreeFloatRatio.Length - 3);

            freeFloatRatio = double.Parse
            (
                firstFreeFloatRatio, CultureInfo.InvariantCulture.NumberFormat
            );

            firstWeightingFactor = firstWeightingFactor.Substring
                    (2, firstWeightingFactor.Length - 2);

            weightingFactor = double.Parse
            (
                firstWeightingFactor, CultureInfo.InvariantCulture.NumberFormat
            );

            firstWeightedFreeFloatMktValue = firstWeightedFreeFloatMktValue.
                Substring(3, firstWeightedFreeFloatMktValue.Length - 3);

            weightedFreeFloatMktValue = double.Parse
            (
                firstWeightedFreeFloatMktValue,
                CultureInfo.InvariantCulture.NumberFormat
            );

            firstMarketCap = firstMarketCap.Substring
                     (3, firstMarketCap.Length - 3);

            marketCap = double.Parse
            (
                firstMarketCap, CultureInfo.InvariantCulture.NumberFormat
            );

            firstSodEod = firstSodEod.Substring
                    (3, firstSodEod.Length - 3);
            sodEod = Int32.Parse(firstSodEod);
        }
    }
}