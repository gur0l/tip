﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TIPlib
{
    public class Orderbook3 : TipMsg
    {
        int number, i;

        public readonly int ID;
        public readonly int sourceSystem;
        public readonly String timeExec;

        public readonly bool wAvgPriceAllBidFlag = false;
        public readonly double wAvgPriceAllBid;

        public readonly bool totVolAllBidFlag = false;
        public readonly double totVolAllBid;

        public readonly bool wAvgPriceAllAskFlag = false;
        public readonly double wAvgPriceAllAsk;

        public readonly bool totVolAllAskFlag = false;
        public readonly double totVolAllAsk;

        public readonly bool orderbookFlushFlag = false;
        public readonly bool orderbookFlush;

        public readonly bool bidLevelDeletedFlag = false;
        public readonly List<int> c = new List<int>();

        public readonly bool askLevelDeletedFlag = false;
        public readonly List<int> e = new List<int>();

        public readonly bool bidPriceDiffFlag = false;
        public readonly double bidPriceDiff;

        public readonly bool bidPriceAtLevelFlag = false;
        public readonly List<double> b = new List<double>();

        public readonly bool bidVolumeAtLevelFlag = false;
        public readonly List<double> g = new List<double>();

        public readonly bool bidOrdersAtLevelFlag = false;
        public readonly List<int> h = new List<int>();

        public readonly bool askPriceAtLevelFlag = false;
        public readonly List<double> a = new List<double>();

        public readonly bool askVolumeAtLevelFlag = false;
        public readonly List<double> j = new List<double>();

        public readonly bool askOrdersAtLevelFlag = false;
        public readonly List<int> k = new List<int>();

        public Orderbook3(string input)
        {
            string[] stringSeparators = new string[] { ";" };
            string[] orderSeparators = new string[] { ":" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);
            string[] orderResult = null;

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceSystem = result[2];
            string firstTimeExec = result[3];

            int x = 0;

            if (input.Contains(";Of"))
            {
                x++;
                orderbookFlushFlag = true;
                orderbookFlush = true;
            }

            string firstBidPriceDiff = null;

            if (input.Contains(";d"))
            {
                x++;
                bidPriceDiffFlag = true;

                firstBidPriceDiff = result[3 + x];

                firstBidPriceDiff = firstBidPriceDiff.TrimStart('d');

                bidPriceDiff = double.Parse
                (
                    firstBidPriceDiff, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstWAvgPriceAllBid = null;
            string firstTotVolAllBid = null;
            string firstWAvgPriceAllAsk = null;
            string firstTotVolAllAsk = null;

            if (input.Contains(";Bw"))
            {
                x++;
                wAvgPriceAllBidFlag = true;

                firstWAvgPriceAllBid = result[3 + x]; ;

                firstWAvgPriceAllBid = firstWAvgPriceAllBid.Substring
                    (2, firstWAvgPriceAllBid.Length - 2);

                wAvgPriceAllBid = double.Parse
                (
                    firstWAvgPriceAllBid, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            if (input.Contains(";Bt"))
            {
                x++;
                totVolAllBidFlag = true;

                firstTotVolAllBid = result[3 + x]; ;

                firstTotVolAllBid = firstTotVolAllBid.Substring
                    (2, firstTotVolAllBid.Length - 2);

                totVolAllBid = double.Parse
                (
                    firstTotVolAllBid, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            if (input.Contains(";Aw"))
            {
                x++;
                wAvgPriceAllAskFlag = true;

                firstWAvgPriceAllAsk = result[3 + x]; ;

                firstWAvgPriceAllAsk = firstWAvgPriceAllAsk.Substring
                    (2, firstWAvgPriceAllAsk.Length - 2);

                wAvgPriceAllAsk = double.Parse
                (
                    firstWAvgPriceAllAsk, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            if (input.Contains(";At"))
            {
                x++;
                totVolAllAskFlag = true;

                firstTotVolAllAsk = result[3 + x]; ;

                firstTotVolAllAsk = firstTotVolAllAsk.Substring
                    (2, firstTotVolAllAsk.Length - 2);

                totVolAllAsk = double.Parse
                (
                    firstTotVolAllAsk, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            timeExec = firstTimeExec.TrimStart('t');

            var builder = new StringBuilder();
            int count = 0;
            foreach (var c in timeExec)
            {
                builder.Append(c);

                if (!(count >= 4))
                {
                    if ((++count % 2) == 0)
                    {
                        builder.Append(':');
                    }
                }
            }
            timeExec = builder.ToString();

            for (i = 0; i < result.Length; i++)
            {
                if (result[i].StartsWith("c"))
                {
                    if (result[i].Contains(":"))
                    {
                        orderResult = result[i].Split(orderSeparators, StringSplitOptions.None);

                        c.Add(Int32.Parse
                            (orderResult[1], CultureInfo.InvariantCulture.NumberFormat));
                    }
                    else
                        c.Add(Int32.Parse(result[i].Substring(1)));
                }
                else if (result[i].StartsWith("e"))
                {
                    if (result[i].Contains(":"))
                    {
                        orderResult = result[i].Split(orderSeparators, StringSplitOptions.None);

                        e.Add(Int32.Parse
                            (orderResult[1], CultureInfo.InvariantCulture.NumberFormat));
                    }
                    else
                        e.Add(Int32.Parse(result[i].Substring(1)));
                }
                else if (result[i].StartsWith("b"))
                {
                    orderResult = result[i].Split(orderSeparators, StringSplitOptions.None);

                    b.Add(double.Parse
                        (orderResult[1], CultureInfo.InvariantCulture.NumberFormat));
                }
                else if (result[i].StartsWith("g"))
                {
                    orderResult = result[i].Split(orderSeparators, StringSplitOptions.None);

                    g.Add(double.Parse
                        (orderResult[1], CultureInfo.InvariantCulture.NumberFormat));
                }
                else if (result[i].StartsWith("h"))
                {
                    orderResult = result[i].Split(orderSeparators, StringSplitOptions.None);

                    a.Add(Int32.Parse
                        (orderResult[1], CultureInfo.InvariantCulture.NumberFormat));
                }
                else if (result[i].StartsWith("a"))
                {
                    orderResult = result[i].Split(orderSeparators, StringSplitOptions.None);

                    a.Add(double.Parse
                        (orderResult[1], CultureInfo.InvariantCulture.NumberFormat));
                }
                else if (result[i].StartsWith("j"))
                {
                    orderResult = result[i].Split(orderSeparators, StringSplitOptions.None);

                    j.Add(double.Parse
                        (orderResult[1], CultureInfo.InvariantCulture.NumberFormat));
                }
                else if (result[i].StartsWith("k"))
                {
                    orderResult = result[i].Split(orderSeparators, StringSplitOptions.None);

                    k.Add(Int32.Parse
                        (orderResult[1], CultureInfo.InvariantCulture.NumberFormat));
                }
            }
        }
    }
}