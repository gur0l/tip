﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class IndexSummary : TipMsg
    {
        public readonly int ID;
        public readonly int sourceSystem;
        public readonly String timeExec;

        public readonly bool closingValueFlag = false;
        public readonly double closingValue;

        public readonly bool highValueFlag = false;
        public readonly double highValue;

        public readonly bool lowValueFlag = false;
        public readonly double lowValue;

        public readonly double accumulatedVolume;
        public readonly double accumulatedTurnover;

        public readonly bool openValueFlag = false;
        public readonly double openValue;

        public readonly bool oldIndexValueFlag = false;
        public readonly double oldIndexValue;

        public readonly double divisor;
        public readonly double diffDayNom;
        public readonly double diffDayPer;
        public readonly double marketCap;
        public readonly int sodEod;

        public IndexSummary(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceSystem = result[2];
            string firstTimeExec = result[3];
            string firstClosingValue = null;

            int x = 0;

            if (input.Contains(";CLv"))
            {
                x++;
                closingValueFlag = true;

                firstClosingValue = result[3 + x];

                firstClosingValue = firstClosingValue.Substring
                    (3, firstClosingValue.Length - 3);

                closingValue = double.Parse
                (
                    firstClosingValue, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstHighValue = null;

            if (input.Contains(";Vh"))
            {
                x++;
                highValueFlag = true;

                firstHighValue = result[3 + x];

                firstHighValue = firstHighValue.Substring
                    (2, firstHighValue.Length - 2);

                highValue = double.Parse
                (
                    firstHighValue, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstLowValue = null;

            if (input.Contains(";Vl"))
            {
                x++;
                lowValueFlag = true;

                firstLowValue = result[3 + x];

                firstLowValue = firstLowValue.Substring
                    (2, firstLowValue.Length - 2);

                lowValue = double.Parse
                (
                    firstLowValue, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstAccumulatedVolume = result[4 + x];
            string firstAccumulatedTurnover = result[5 + x];
            string firstOpenValue = null;

            if (input.Contains(";OVa"))
            {
                x++;
                openValueFlag = true;

                firstOpenValue = result[5 + x];

                firstOpenValue = firstOpenValue.Substring
                    (3, firstOpenValue.Length - 3);

                openValue = double.Parse
                (
                    firstOpenValue, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstOldIndexValue = null;

            if (input.Contains(";ODXv"))
            {
                x++;
                oldIndexValueFlag = true;

                firstOldIndexValue = result[5 + x];

                firstOldIndexValue = firstOldIndexValue.Substring
                    (4, firstOldIndexValue.Length - 4);

                oldIndexValue = double.Parse
                (
                    firstOldIndexValue, CultureInfo.InvariantCulture.NumberFormat
                );

            }

            string firstDivisor = result[6 + x];
            string firstDiffDayNom = result[7 + x];
            string firstDiffDayPer = result[8 + x];
            string firstMarketCap = result[9 + x];
            string firstSodEod = result[10 + x];

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            timeExec = firstTimeExec.TrimStart('t');

            var builder = new StringBuilder();
            int count = 0;
            foreach (var c in timeExec)
            {
                builder.Append(c);

                if (!(count >= 4))
                {
                    if ((++count % 2) == 0)
                    {
                        builder.Append(':');
                    }
                }
            }
            timeExec = builder.ToString();

            firstAccumulatedVolume = firstAccumulatedVolume.TrimStart('o');
            accumulatedVolume = double.Parse
            (
                firstAccumulatedVolume, CultureInfo.InvariantCulture.NumberFormat
            );

            firstAccumulatedTurnover = firstAccumulatedTurnover.TrimStart('f');
            accumulatedTurnover = double.Parse
            (
                firstAccumulatedTurnover, CultureInfo.InvariantCulture.NumberFormat
            );

            firstDivisor = firstDivisor.Substring
                    (4, firstDivisor.Length - 4);

            divisor = double.Parse
            (
                firstDivisor, CultureInfo.InvariantCulture.NumberFormat
            );

            firstDiffDayNom = firstDiffDayNom.Substring
                    (2, firstDiffDayNom.Length - 2);

            diffDayNom = double.Parse
            (
                firstDiffDayNom, CultureInfo.InvariantCulture.NumberFormat
            );

            firstDiffDayPer = firstDiffDayPer.Substring
                    (2, firstDiffDayPer.Length - 2);

            diffDayPer = double.Parse
            (
                firstDiffDayPer, CultureInfo.InvariantCulture.NumberFormat
            );

            firstMarketCap = firstMarketCap.Substring
                    (3, firstMarketCap.Length - 3);

            marketCap = double.Parse
            (
                firstMarketCap, CultureInfo.InvariantCulture.NumberFormat
            );

            firstSodEod = firstSodEod.Substring
                    (3, firstSodEod.Length - 3);
            sodEod = Int32.Parse(firstSodEod);
        }
    }
}