﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class BasicDataParticipant : TipMsg
    {
        public readonly int ID;
        public readonly String sourceID;
        public readonly int sourceSystem;
        public readonly String symbol;
        public readonly String name;
        public readonly int participantType;
        public readonly String bicCode;
        public readonly bool bicFlag = false;
        public readonly char grossSettlement;

        public BasicDataParticipant(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceID = result[2];
            string firstSourceSystem = result[3];
            string firstSymbol = result[4];
            string firstName = result[5];
            string firstParticipantType = result[6];
            string firstBicCode = null;
            string firstGrossSettlement = null;

            if (input.Contains(";BIc"))
            {
                bicFlag = true;

                firstBicCode = result[7];
                firstGrossSettlement = result[8];

                bicCode = firstBicCode.Substring
                (
                    3, firstBicCode.Length - 3
                );
            }
            else
            {
                firstGrossSettlement = result[7];
            }

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            sourceID = firstSourceID.Substring(2, firstSourceID.Length - 2);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            symbol = firstSymbol.Substring
            (
                3, firstSymbol.Length - 3
            );

            name = firstName.Substring(3, firstName.Length - 3);

            firstParticipantType = firstParticipantType.Substring
            (
                3, firstParticipantType.Length - 3
            );
            participantType = Int32.Parse(firstParticipantType);

            firstGrossSettlement = firstGrossSettlement.Substring
            (
                3, firstGrossSettlement.Length - 3
            );
            grossSettlement = char.Parse(firstGrossSettlement);
        }
    }
}