﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class TradeStatistics1 : TipMsg
    {
        public readonly int ID;
        public readonly int sourceSystem;
        public readonly String timeExec;

        public readonly bool lastPriceFlag = false;
        public readonly double lastPrice;

        public readonly bool diffLastPriceFlag = false;
        public readonly double diffLastPrice;

        public readonly bool orderbookFlushFlag = false;
        public readonly bool orderbookFlush;

        public readonly bool diffDayPerFlag = false;
        public readonly double diffDayPer;

        public TradeStatistics1(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceSystem = result[2];
            string firstTimeExec = result[3];
            string firstLastPrice = null;
            string firstDiffLastPrice = null;
            string firstDiffDayPer = null;

            int x = 0;
  
            if (input.Contains(";Pl"))
            {
                x++;
                lastPriceFlag = true;

                firstLastPrice = result[3+x];

                firstLastPrice = firstLastPrice.Substring
                (
                    2, firstLastPrice.Length - 2
                );

                lastPrice = double.Parse
                (
                    firstLastPrice,
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            if (input.Contains(";Pd"))
            {
                x++;
                diffLastPriceFlag = true;

                firstDiffLastPrice = result[3 + x];

                firstDiffLastPrice = firstDiffLastPrice.Substring
                (
                    2, firstDiffLastPrice.Length - 2
                );

                diffLastPrice = double.Parse
                (
                    firstDiffLastPrice,
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            if (input.Contains(";Of"))
            {
                x++;
                orderbookFlushFlag = true;
                orderbookFlush = true;
            }

            if (input.Contains(";Dd"))
            {
                x++;
                diffDayPerFlag = true;

                firstDiffDayPer = result[3 + x];

                firstDiffDayPer = firstDiffDayPer.Substring
                (
                    2, firstDiffDayPer.Length - 2
                );

                diffDayPer = double.Parse
                (
                    firstDiffDayPer,
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            timeExec = firstTimeExec.TrimStart('t');

            var builder = new StringBuilder();
            int count = 0;
            foreach (var c in timeExec)
            {
                builder.Append(c);

                if (!(count >= 4))
                {
                    if ((++count % 2) == 0)
                    {
                        builder.Append(':');
                    }
                }
            }
            timeExec = builder.ToString();
        }
    }
}