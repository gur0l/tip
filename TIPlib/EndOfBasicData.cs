﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class EndOfBasicData : TipMsg
    {
        public readonly int sourceSystem;

        public EndOfBasicData(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstSourceSystem = result[1];

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);
        }
    }
}