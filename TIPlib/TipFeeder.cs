﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class TipFeeder
    {
        public static string[] ReadFile(string fileName)
        {
            // Read the file and display it line by line.
            return File.ReadAllLines(fileName);
        }
    }
}
