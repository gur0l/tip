﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class BasicDataDerivative : TipMsg
    {
        public readonly int ID;
        public readonly String sourceID;
        public readonly int sourceSystem;
        public readonly int derivativeType;

        public readonly bool totalIssueFlag = false;
        public readonly double totalIssue;

        public readonly int exerciseType;

        public readonly bool strikePriceFlag = false;
        public readonly double strikePrice;

        public readonly double contractSize;
        public readonly int settlementType;

        public readonly bool exerciseFromDateFlag = false;
        public readonly DateTime exerciseFromDate;

        public readonly DateTime exerciseToDate;

        public readonly bool settlementDateFlag = false;
        public readonly DateTime settlementDate;

        public readonly bool hotInsertedFlag = false;
        public readonly bool hotInserted;

        public BasicDataDerivative(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceID = result[2];
            string firstSourceSystem = result[3];
            string firstDerivativeType = result[4];
            string firstTotalIssue = null;

            int x = 0;

            if (input.Contains(";TIs"))
            {
                x++;
                totalIssueFlag = true;

                firstTotalIssue = result[4 + x];

                firstTotalIssue = firstTotalIssue.Substring
                (
                    3, firstTotalIssue.Length - 3
                );

                totalIssue = double.Parse
                (
                    firstTotalIssue, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstExerciseType = result[5 + x];
            string firstStrikePrice = null;

            if (input.Contains(";STp"))
            {
                x++;
                strikePriceFlag = true;

                firstStrikePrice = result[5 + x];

                firstStrikePrice = firstStrikePrice.Substring
                (
                    3, firstStrikePrice.Length - 3
                );

                strikePrice = double.Parse
                (
                    firstStrikePrice, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstContractSize = result[6 + x];
            string firstSettlementType = result[7 + x];
            string firstExerciseFromDate = null;

            if (input.Contains(";EXb"))
            {
                x++;
                exerciseFromDateFlag = true;

                firstExerciseFromDate = result[7 + x];

                firstExerciseFromDate =
                    firstExerciseFromDate.Substring
                    (3, firstExerciseFromDate.Length - 3);

                exerciseFromDate = DateTime.ParseExact
                (
                    firstExerciseFromDate, "yyyyMMdd",
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            string firstExerciseToDate = result[8 + x];
            string firstSettlementDate = null;

            if (input.Contains(";Sd"))
            {
                x++;
                settlementDateFlag = true;

                firstSettlementDate = result[8 + x];

                firstSettlementDate =
                    firstSettlementDate.Substring
                    (2, firstSettlementDate.Length - 2);

                settlementDate = DateTime.ParseExact
                (
                    firstSettlementDate, "yyyyMMdd",
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            //string firstHotInserted = null;

            if (input.Contains(";HOt"))
            {
                x++;
                hotInsertedFlag = true;
                hotInserted = true;
            }

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            sourceID = firstSourceID.Substring(2, firstSourceID.Length - 2);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            firstDerivativeType = firstDerivativeType.Substring
                (3, firstDerivativeType.Length - 3);
            derivativeType = Int32.Parse(firstDerivativeType);

            firstExerciseType = firstExerciseType.Substring
                (3, firstExerciseType.Length - 3);
            exerciseType = Int32.Parse(firstExerciseType);

            firstContractSize = firstContractSize.Substring
                (3, firstContractSize.Length - 3);
            contractSize = double.Parse
            (
                firstContractSize, CultureInfo.InvariantCulture.NumberFormat
            );

            firstSettlementType = firstSettlementType.Substring
                (3, firstSettlementType.Length - 3);
            settlementType = Int32.Parse(firstSettlementType);

            firstExerciseToDate = firstExerciseToDate.Substring
                (3, firstExerciseToDate.Length - 3);

            exerciseToDate = DateTime.ParseExact
            (
                firstExerciseToDate, "yyyyMMdd",
                System.Globalization.CultureInfo.InvariantCulture
            );
        }
    }
}