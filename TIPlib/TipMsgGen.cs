﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class TipMsgGen
    {
        public static TipMsg generateTipMsg(string input)
        {
            TipMsg myMessage = null;
            string operation = input.Substring(0,input.IndexOf(';'));

            if(operation == "BDBu")
            {
                myMessage = new BasicDataBusinessDate(input);
            }
            else if (operation == "BDCv")
            {
                myMessage = new BasicDataClearingVenue(input);
            }
            else if (operation == "BDDe")
            {
                myMessage = new BasicDataDerivative(input);
            }
            else if (operation == "BDx")
            {
                myMessage = new BasicDataExchange(input);
            }
            else if (operation == "BDEt")
            {
                myMessage = new BasicDataFund(input);
            }
            else if (operation == "BDIm")
            {
                myMessage = new BasicDataIndexMember(input);
            }
            else if (operation == "BDIp")
            {
                myMessage = new BasicDataIndexSupplementary(input);
            }
            else if (operation == "BDIn")
            {
                myMessage = new BasicDataIndex(input);
            }
            else if (operation == "BDIs")
            {
                myMessage = new BasicDataIssuer(input);
            }
            else if (operation == "BDLm")
            {
                myMessage = new BasicDataListMember(input);
            }
            else if (operation == "BDLi")
            {
                myMessage = new BasicDataList(input);
            }
            else if (operation == "BDm")
            {
                myMessage = new BasicDataMarket(input);
            }
            else if (operation == "BDTd")
            {
                myMessage = new BasicDataNonTradingDays(input);
            }
            else if (operation == "BDLm")
            {
                myMessage = new BasicDataParticipant(input);
            }
            else if (operation == "BDRi")
            {
                myMessage = new BasicDataRight(input);
            }
            else if (operation == "BDSm")
            {
                myMessage = new BasicDataSectorMember(input);
            }
            else if (operation == "BDs")
            {
                myMessage = new BasicDataSector(input);
            }
            else if (operation == "BDSh")
            {
                myMessage = new BasicDataShare(input);
            }
            else if (operation == "BDSr")
            {
                myMessage = new BasicDataSource(input);
            }
            else if (operation == "BDTe")
            {
                myMessage = new BasicDataTableEntry(input);
            }
            else if (operation == "BDTz")
            {
                myMessage = new BasicDataTickSizeEntry(input);
            }
            else if (operation == "BDTs")
            {
                myMessage = new BasicDataTickSizeTable(input);
            }
            else if (operation == "BDTr")
            {
                myMessage = new BasicDataTradableSupplementary(input);
            }
            else if (operation == "BDt")
            {
                myMessage = new BasicDataTradable(input);
            }
            else if (operation == "BDTm")
            {
                myMessage = new BasicDataTradingScheme(input);
            }
            else if (operation == "BDUi")
            {
                myMessage = new BasicDataUnderlyingInfo(input);
            }
            else if (operation == "c")
            {
                myMessage = new CallInformation1(input);
            }
            else if (operation == "Cl")
            {
                myMessage = new CallInformation2(input);
            }
            else if (operation == "TRh")
            {
                myMessage = new CorporateAction(input);
            }
            else if (operation == "EOBd")
            {
                myMessage = new EndOfBasicData(input);
            }
            else if (operation == "Is")
            {
                myMessage = new IndexSummary(input);
            }
            else if (operation == "i")
            {
                myMessage = new IndexUpdate(input);
            }
            else if (operation == "Iw")
            {
                myMessage = new IndexWeight(input);
            }
            else if (operation == "q")
            {
                myMessage = new MarketMakerQuote1(input);
            }
            else if (operation == "y")
            {
                myMessage = new MarketMakerQuote2(input);
            }
            else if (operation == "k")
            {
                myMessage = new MBPOrderSnapshot(input);
            }
            else if (operation == "n")
            {
                myMessage = new News(input);
            }
            else if (operation == "o")
            {
                myMessage = new Orderbook1(input);
            }
            else if (operation == "p")
            {
                myMessage = new Orderbook2(input);
            }
            else if (operation == "z")
            {
                myMessage = new Orderbook3(input);
            }
            else if (operation == "r")
            {
                myMessage = new OrderbookReferencePrice(input);
            }
            else if (operation == "m")
            {
                myMessage = new OrderbookSummary(input);
            }
            else if (operation == "s")
            {
                myMessage = new StateChange(input);
            }
            else if (operation == "u")
            {
                myMessage = new TradeStatistics1(input);
            }
            else if (operation == "v")
            {
                myMessage = new TradeStatistics2(input);
            }
            else if (operation == "w")
            {
                myMessage = new TradeStatistics3(input);
            }
            else if (operation == "t")
            {
                myMessage = new Trade(input);
            }
            else if (operation == "l")
            {
                myMessage = new TurnoverListUpdate(input);
            }

            return myMessage;
        }
    }
}