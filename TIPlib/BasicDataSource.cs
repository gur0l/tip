﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class BasicDataSource : TipMsg
    {
        public readonly int ID;
        public readonly String sourceData;

        public BasicDataSource(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstData = result[2];

            firstID = firstID.TrimStart('i');

            ID = Int32.Parse(firstID);
            sourceData = firstData.Substring(3, firstData.Length - 3);
        }
    }
}