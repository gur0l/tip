﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIPlib;

namespace TIPproject
{
    class SoupBinEncapsulator
    {
        string[] LineArray = TipFeeder.ReadFile("tip_20150730_all.log");

        public SoupBinEncapsulator()
        {
            SequencedDataPacket toBeWritten;

            using (var stream = new FileStream("out.log", FileMode.Append))
            {
                for (int i = 0; i < LineArray.Length; i++)
                {
                    toBeWritten = new SequencedDataPacket(LineArray[i]);

                    stream.Write(toBeWritten.readyPacket, 0, (toBeWritten.readyPacket).Length);
                }
            }
        }
    }
}
