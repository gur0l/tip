﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class BasicDataSectorMember : TipMsg
    {
        public readonly int orderbookID;
        public readonly int sectorID;
        public readonly int sourceSystem;

        public BasicDataSectorMember(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstOrderbookID = result[1];
            string firstSectorID = result[2];
            string firstSourceSystem = result[3];

            firstOrderbookID = firstOrderbookID.Substring
            (
                3, firstOrderbookID.Length - 3
            );
            orderbookID = Int32.Parse(firstOrderbookID);

            firstSectorID = firstSectorID.Substring
            (
                3, firstSectorID.Length - 3
            );
            sectorID = Int32.Parse(firstSectorID);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);
        }
    }
}