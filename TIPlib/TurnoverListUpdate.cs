﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class TurnoverListUpdate : TipMsg
    {
        public readonly int ID;
        public readonly int sourceSystem;
        public readonly String timeExec;
        public readonly double accumulatedTurnover;
        public readonly double accumulatedVolume;
        public readonly int unchangedBids;

        public readonly bool plusBidsFlag = false;
        public readonly int plusBids;

        public readonly bool minusBidsFlag = false;
        public readonly int minusBids;

        public readonly int unchangedPaid;

        public readonly bool plusPaidFlag = false;
        public readonly int plusPaid;

        public readonly bool minusPaidFlag = false;
        public readonly int minusPaid;

        public readonly int totalNumberOfTrades;

        public TurnoverListUpdate(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceSystem = result[2];
            string firstTimeExec = result[3];
            string firstAccumulatedTurnover = result[4];
            string firstAccumulatedVolume = result[5];
            string firstUnchangedBids = result[6];

            string firstPlusBids = null;
            string firstMinusBids = null;

            int x = 0;

            if (input.Contains(";Bp"))
            {
                x++;
                plusBidsFlag = true;

                firstPlusBids = result[6 + x];

                firstPlusBids = firstPlusBids.Substring
                    (2, firstPlusBids.Length - 2);

                plusBids = Int32.Parse(firstPlusBids);
            }

            if (input.Contains(";Bm"))
            {
                x++;
                minusBidsFlag = true;

                firstMinusBids = result[6 + x];

                firstMinusBids = firstMinusBids.Substring
                    (2, firstMinusBids.Length - 2);

                minusBids = Int32.Parse(firstMinusBids);
            }

            string firstUnchangedPaid = result[7 + x];
            string firstPlusPaid = null;
            string firstMinusPaid = null;

            if (input.Contains(";Pp"))
            {
                x++;
                plusPaidFlag = true;

                firstPlusPaid = result[7 + x];

                firstPlusPaid = firstPlusPaid.Substring
                    (2, firstPlusPaid.Length - 2);
                plusPaid = Int32.Parse(firstPlusPaid);
            }

            if (input.Contains(";Pm"))
            {
                x++;
                minusPaidFlag = true;

                firstMinusPaid = result[7 + x];

                firstMinusPaid = firstMinusPaid.Substring
                    (2, firstMinusPaid.Length - 2);
                minusPaid = Int32.Parse(firstMinusPaid);
            }

            string firstTotalNumberOfTrades = result[8 + x];

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            timeExec = firstTimeExec.TrimStart('t');

            var builder = new StringBuilder();
            int count = 0;
            foreach (var c in timeExec)
            {
                builder.Append(c);

                if (!(count >= 4))
                {
                    if ((++count % 2) == 0)
                    {
                        builder.Append(':');
                    }
                }
            }
            timeExec = builder.ToString();

            firstAccumulatedTurnover = firstAccumulatedTurnover.TrimStart('f');
            accumulatedTurnover = double.Parse
            (
                firstAccumulatedTurnover, CultureInfo.InvariantCulture.NumberFormat
            );

            firstAccumulatedVolume = firstAccumulatedVolume.TrimStart('o');
            accumulatedVolume = double.Parse
            (
                firstAccumulatedVolume, CultureInfo.InvariantCulture.NumberFormat
            );

            firstUnchangedBids = firstUnchangedBids.Substring
                    (2, firstUnchangedBids.Length - 2);
            unchangedBids = Int32.Parse(firstUnchangedBids);

            firstUnchangedPaid = firstUnchangedPaid.Substring
                    (2, firstUnchangedPaid.Length - 2);
            unchangedPaid = Int32.Parse(firstUnchangedPaid);

            firstTotalNumberOfTrades = firstTotalNumberOfTrades.Substring
                    (3, firstTotalNumberOfTrades.Length - 3);
            totalNumberOfTrades = Int32.Parse(firstTotalNumberOfTrades);
        }
    }
}