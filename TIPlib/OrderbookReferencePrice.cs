﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class OrderbookReferencePrice : TipMsg
    {
        public readonly int sourceSystem;
        public readonly int ID;
        public readonly String timeExec;
        public readonly double basePrice;

        public readonly bool additionalReferencePriceFlag = false;
        public readonly double additionalReferencePrice;

        public readonly bool lowerPriceLimitFlag = false;
        public readonly double lowerPriceLimit;

        public readonly bool upperPriceLimitFlag = false;
        public readonly double upperPriceLimit;

        public OrderbookReferencePrice(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstSourceSystem = result[1];
            string firstID = result[2];
            string firstTimeExec = result[3];
            string firstBasePrice = result[4];
            string firstAdditionalReferencePrice = null;

            int x = 0;

            if (input.Contains(";ARp"))
            {
                additionalReferencePriceFlag = true;

                firstAdditionalReferencePrice = result[4 + x];

                firstAdditionalReferencePrice = firstAdditionalReferencePrice.
                    Substring(3, firstAdditionalReferencePrice.Length - 3);

                additionalReferencePrice = double.Parse
                 (firstAdditionalReferencePrice, 
                 CultureInfo.InvariantCulture.NumberFormat);
            }

            string firstLowerPriceLimit = null;
            string firstUpperPriceLimit = null;

            if (input.Contains(";LPl"))
            {
                lowerPriceLimitFlag = true;

                firstLowerPriceLimit = result[4 + x];

                firstLowerPriceLimit = firstLowerPriceLimit.Substring
                    (3, firstLowerPriceLimit.Length - 3);

                lowerPriceLimit = double.Parse
                    (firstLowerPriceLimit, CultureInfo.InvariantCulture.NumberFormat);
            }

            if (input.Contains(";UPl"))
            {
                upperPriceLimitFlag = true;

                firstUpperPriceLimit = result[4 + x];

                firstUpperPriceLimit = firstUpperPriceLimit.Substring
                    (3, firstUpperPriceLimit.Length - 3);

                upperPriceLimit = double.Parse
                    (firstUpperPriceLimit, CultureInfo.InvariantCulture.NumberFormat);
            }

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            timeExec = firstTimeExec.TrimStart('t');

            var builder = new StringBuilder();
            int count = 0;
            foreach (var c in timeExec)
            {
                builder.Append(c);

                if (!(count >= 4))
                {
                    if ((++count % 2) == 0)
                    {
                        builder.Append(':');
                    }
                }
            }
            timeExec = builder.ToString();

            firstBasePrice = firstBasePrice.Substring
                (3, firstBasePrice.Length - 3);

            basePrice = double.Parse
                (firstBasePrice,CultureInfo.InvariantCulture.NumberFormat);
        }
    }
}