﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class BasicDataListMember : TipMsg
    {
        public readonly int orderbookID;
        public readonly int listID;
        public readonly int sourceSystem;
        public readonly bool hotInsertedFlag = false;
        public readonly bool hotInserted;

        public BasicDataListMember(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstOrderbookID = result[1];
            string firstListID = result[2];
            string firstSourceSystem = result[3];
            string firstHotInserted = null;

            firstOrderbookID = firstOrderbookID.Substring
            (
                3, firstOrderbookID.Length - 3
            );
            orderbookID = Int32.Parse(firstOrderbookID);

            firstListID = firstListID.Substring
            (
                3, firstListID.Length - 3
            );
            listID = Int32.Parse(firstListID);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            if (input.Contains(";HOt"))
            {
                hotInsertedFlag = true;
                hotInserted = true;
            }
        }
    }
}