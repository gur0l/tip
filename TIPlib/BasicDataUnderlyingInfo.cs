﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class BasicDataUnderlyingInfo : TipMsg
    {
        public readonly int ID;
        public readonly String sourceID;
        public readonly int sourceSystem;

        public readonly bool underlyingIdFlag = false;
        public readonly int underlyingId;

        public readonly String description;

        public readonly bool hotInsertedFlag = false;
        public readonly bool hotInserted;

        public BasicDataUnderlyingInfo(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceID = result[2];
            string firstSourceSystem = result[3];
            string firstUnderlyingId = null;

            int x = 0;

            if (input.Contains(";ULi"))
            {
                x++;
                underlyingIdFlag = true;

                firstUnderlyingId = result[3 + x];

                firstUnderlyingId = firstUnderlyingId.Substring
                (
                    3, firstUnderlyingId.Length - 3
                );

                underlyingId = Int32.Parse
                (
                    firstUnderlyingId
                );
            }

            string firstDescription = result[4 + x];
            string firstHotInserted = null;

            if (input.Contains(";HOt"))
            {
                x++;
                hotInsertedFlag = true;
                hotInserted = true;
            }

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            sourceID = firstSourceID.Substring(2, firstSourceID.Length - 2);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            description = firstDescription.Substring
                (3, firstDescription.Length - 3);
        }
    }
}