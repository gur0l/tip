﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class BasicDataNonTradingDays : TipMsg
    {
        public readonly int ID;
        public readonly String sourceID;
        public readonly int sourceSystem;
        public readonly int dayType;
        public readonly DateTime date;

        public BasicDataNonTradingDays(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstSourceSystem = result[1];
            string firstID = result[2];
            string firstSourceID = result[3];
            string firstDate = result[4];
            string firstDayType = result[5];

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            sourceID = firstSourceID.Substring(2, firstSourceID.Length - 2);

            firstDate = firstDate.Substring
            (
                2, firstDate.Length - 2
            );

            date = DateTime.ParseExact
            (
                firstDate, "yyyyMMdd",
                System.Globalization.CultureInfo.InvariantCulture
            );

            firstDayType = firstDayType.Substring
            (
                3, firstDayType.Length - 3
            );
            dayType = Int32.Parse(firstDayType);
        }
    }
}