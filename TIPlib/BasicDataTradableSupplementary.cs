﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class BasicDataTradableSupplementary : TipMsg
    {
        public readonly int ID;
        public readonly String sourceID;
        public readonly int sourceSystem;
        public readonly String ISIN;

        public BasicDataTradableSupplementary(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceID = result[2];
            string firstSourceSystem = result[3];
            string firstISIN = result[4];

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            sourceID = firstSourceID.Substring(2, firstSourceID.Length - 2);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            ISIN = firstISIN.Substring(3, firstISIN.Length - 3);
        }
    }
}