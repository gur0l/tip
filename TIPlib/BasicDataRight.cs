﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class BasicDataRight : TipMsg
    {
        public readonly int ID;
        public readonly String sourceID;
        public readonly int sourceSystem;

        public readonly bool contractSizeFlag = false;
        public readonly double contractSize;

        public readonly bool exerciseFromDateFlag = false;
        public readonly DateTime exerciseFromDate;

        public readonly bool exerciseToDateFlag = false;
        public readonly DateTime exerciseToDate;

        public readonly bool totalIssueFlag = false;
        public readonly double totalIssue;

        public readonly String exerciseCurrency;

        public readonly bool hotInsertedFlag = false;
        public readonly bool hotInserted;

        public BasicDataRight(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceID = result[2];
            string firstSourceSystem = result[3];
            string firstContractSize = null;

            int x = 0;

            if (input.Contains(";CSz"))
            {
                x++;
                contractSizeFlag = true;

                firstContractSize = result[3 + x];

                firstContractSize = firstContractSize.Substring
                (
                    3, firstContractSize.Length - 3
                );

                contractSize = double.Parse
                (
                    firstContractSize, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstExerciseFromDate = null;

            if (input.Contains(";EXb"))
            {
                x++;
                exerciseFromDateFlag = true;

                firstExerciseFromDate = result[3 + x];

                firstExerciseFromDate =
                    firstExerciseFromDate.Substring
                    (3, firstExerciseFromDate.Length - 3);

                exerciseFromDate = DateTime.ParseExact
                (
                    firstExerciseFromDate, "yyyyMMdd",
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            string firstExerciseToDate = null;

            if (input.Contains(";EXe"))
            {
                x++;
                exerciseToDateFlag = true;

                firstExerciseToDate = result[3 + x];

                firstExerciseToDate =
                    firstExerciseToDate.Substring
                    (3, firstExerciseToDate.Length - 3);

                exerciseToDate = DateTime.ParseExact
                (
                    firstExerciseToDate, "yyyyMMdd",
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            string firstTotalIssue = null;

            if (input.Contains(";TIs"))
            {
                x++;
                totalIssueFlag = true;

                firstTotalIssue = result[3 + x];

                firstTotalIssue =
                    firstTotalIssue.Substring
                    (2, firstTotalIssue.Length - 2);

                totalIssue = double.Parse
                (
                    firstTotalIssue, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstExerciseCurrency = result[4 + x];
            string firstHotInserted = null;

            if (input.Contains(";HOt"))
            {
                x++;
                hotInsertedFlag = true;
                hotInserted = true;
            }

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            sourceID = firstSourceID.Substring(2, firstSourceID.Length - 2);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            exerciseCurrency = firstExerciseCurrency.Substring
                (3, firstExerciseCurrency.Length - 3);
        }
    }
}