﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class SequencedDataPacket
    {
        public readonly byte[] readyPacket;
        public readonly byte packetType = (byte)'S';
        public readonly short packetLength;

        public SequencedDataPacket(string message)
        {
            byte[] toBytes = Encoding.ASCII.GetBytes(message);
            readyPacket = new byte[(toBytes.Length) + 3];

            packetLength = (short)(toBytes.Length + 1);

            byte[] lengthInBytes = BitConverter.GetBytes(packetLength);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(lengthInBytes);
            byte[] result = lengthInBytes;

            Array.Copy(result, 0, readyPacket, 0, result.Length);
            readyPacket[2] = packetType;
            Array.Copy(toBytes, 0, readyPacket, 3, toBytes.Length);
        }
    }
}