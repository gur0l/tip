﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class StateChange : TipMsg
    {
        public readonly int ID;
        public readonly int sourceSystem;
        public readonly String timeExec;
        public readonly int stateCode;
        public readonly int stateLevel;

        public readonly bool orderbookFlushFlag = false;
        public readonly bool orderbookFlush;

        public StateChange(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceSystem = result[2];
            string firstTimeExec = result[3];
            string firstStateCode = result[4];
            string firstStateLevel = result[5];

            if (input.Contains(";Of"))
            {
                orderbookFlushFlag = true;
                orderbookFlush = true;
            }

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            firstStateCode = firstStateCode.Substring
                    (2, firstStateCode.Length - 2);
            stateCode = Int32.Parse(firstStateCode);

            firstStateLevel = firstStateLevel.Substring
                    (2, firstStateLevel.Length - 2);
            stateLevel = Int32.Parse(firstStateLevel);

            timeExec = firstTimeExec.TrimStart('t');

            var builder = new StringBuilder();
            int count = 0;
            foreach (var c in timeExec)
            {
                builder.Append(c);

                if (!(count >= 4))
                {
                    if ((++count % 2) == 0)
                    {
                        builder.Append(':');
                    }
                }
            }
            timeExec = builder.ToString();
        }
    }
}