﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TIPlib
{
    public class MBPOrderSnapshot : TipMsg
    {
        Match match;// = regex.Match(input);
        int number, i;

        public readonly int sourceSystem;
        public readonly int ID;
        public readonly String timeExec;

        public readonly bool rankedBidPriceFlag = false;
        public readonly List<double> RBp = new List<double>();

        public readonly bool rankedBidVolumeFlag = false;
        public readonly List<double> RBv = new List<double>();

        public readonly bool rankedAskPriceFlag = false;
        public readonly List<double> RAp = new List<double>();

        public readonly bool rankedAskVolumeFlag = false;
        public readonly List<double> RAv = new List<double>();

        public MBPOrderSnapshot(string input)
        {
            string[] stringSeparators = new string[] { ";" };
            string[] orderSeparators = new string[] { ":" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);
            string[] orderResult;

            //string operation = result[0];
            string firstSourceSystem = result[1];
            string firstID = result[2];
            string firstTimeExec = result[3];

            int x = 0;

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            timeExec = firstTimeExec.TrimStart('t');

            var builder = new StringBuilder();
            int count = 0;
            foreach (var c in timeExec)
            {
                builder.Append(c);

                if (!(count >= 4))
                {
                    if ((++count % 2) == 0)
                    {
                        builder.Append(':');
                    }
                }
            }
            timeExec = builder.ToString();

            for (i = 0; i < result.Length; i++)
            {
                if (result[i].StartsWith("RBp"))
                {
                    orderResult = result[i].Split(orderSeparators, StringSplitOptions.None);

                    RBp.Add(double.Parse
                        (orderResult[1], CultureInfo.InvariantCulture.NumberFormat));
                }
                else if (result[i].StartsWith("RBv"))
                {
                    orderResult = result[i].Split(orderSeparators, StringSplitOptions.None);

                    RBv.Add(double.Parse
                        (orderResult[1], CultureInfo.InvariantCulture.NumberFormat));
                }
                else if (result[i].StartsWith("RAp"))
                {
                    orderResult = result[i].Split(orderSeparators, StringSplitOptions.None);

                    RAp.Add(double.Parse
                        (orderResult[1], CultureInfo.InvariantCulture.NumberFormat));
                }
                else if (result[i].StartsWith("RAv"))
                {
                    orderResult = result[i].Split(orderSeparators, StringSplitOptions.None);

                    RAv.Add(double.Parse
                        (orderResult[1], CultureInfo.InvariantCulture.NumberFormat));
                }
            }
        }
    }
}