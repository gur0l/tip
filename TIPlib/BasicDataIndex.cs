﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class BasicDataIndex : TipMsg
    {
        public readonly int ID;
        public readonly String sourceID;
        public readonly int sourceSystem;
        public readonly String symbol;
        public readonly String name;
        public readonly String currency;

        public readonly bool populationTypeFlag = false;
        public readonly int populationType;

        public readonly bool calculationTypeFlag = false;
        public readonly int calculationType;

        public readonly bool indexTypeFlag = false;
        public readonly int indexType;

        public readonly bool indexPriceTypeFlag = false;
        public readonly int indexPriceType;

        public readonly int indexStatus;

        public readonly bool indexOwnerFlag = false;
        public readonly int indexOwner;

        public readonly int disseminationInterval;
        
        public readonly bool hotInsertedFlag = false;
        public readonly bool hotInserted;

        public readonly bool sectorIDFlag = false;
        public readonly int sectorID;

        public BasicDataIndex(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            int x = 0;

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceID = result[2];
            string firstSourceSystem = result[3];
            string firstSymbol = result[4];
            string firstName = result[5];
            string firstCurrency = result[6];

            string firstPopulationType = null;
            string firstCalculationType = null;
            string firstIndexType = null;
            string firstIndexPriceType = null;

            if (input.Contains(";POt"))
            {
                x++;
                populationTypeFlag = true;

                firstPopulationType = result[6 + x];

                firstPopulationType = firstPopulationType.Substring
                    (3, firstPopulationType.Length - 3);
                populationType = Int32.Parse(firstPopulationType);
            }

            if (input.Contains(";TYc"))
            {
                x++;
                calculationTypeFlag = true;

                firstCalculationType = result[6 + x];

                firstCalculationType = firstCalculationType.Substring
                    (3, firstCalculationType.Length - 3);
                calculationType = Int32.Parse(firstCalculationType);
            }

            if (input.Contains(";ITy"))
            {
                x++;
                indexTypeFlag = true;

                firstIndexType = result[6 + x];

                firstIndexType = firstIndexType.Substring
                    (3, firstIndexType.Length - 3);
                indexType = Int32.Parse(firstIndexType);
            }

            if (input.Contains(";IPt"))
            {
                x++;
                indexPriceTypeFlag = true;

                firstIndexPriceType = result[6 + x];

                firstIndexPriceType = firstIndexPriceType.Substring
                    (3, firstIndexPriceType.Length - 3);
                indexPriceType = Int32.Parse(firstIndexPriceType);
            }

            string firstIndexStatus = result[7 + x];
            string firstIndexOwner = null;

            if (input.Contains(";Io"))
            {
                x++;
                indexOwnerFlag = true;

                firstIndexOwner = result[7 + x];

                firstIndexOwner = firstIndexOwner.Substring
                    (2, firstIndexOwner.Length - 2);
                indexOwner = Int32.Parse(firstIndexOwner);
            }

            string firstDisseminationInterval = result[8 + x];
            //string firstHotInserted = null;

            if (input.Contains(";HOt"))
            {
                x++;
                hotInsertedFlag = true;
                hotInserted = true;
            }

            string firstSectorID = null;

            if (input.Contains(";SId"))
            {
                x++;
                sectorIDFlag = true;

                firstSectorID = result[8 + x];

                firstSectorID = firstSectorID.Substring
                (
                    3, firstSectorID.Length - 3
                );

                sectorID = Int32.Parse(firstSectorID);
            }

            // TOMORROW IS WHEN THIS IS OVER.

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            sourceID = firstSourceID.Substring(2, firstSourceID.Length - 2);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            symbol = firstSymbol.Substring(3, firstSymbol.Length - 3);

            name = firstName.Substring(3, firstName.Length - 3);

            currency = firstCurrency.Substring(3, firstCurrency.Length - 3);

            firstIndexStatus = firstIndexStatus.Substring
                (2, firstIndexStatus.Length - 2);
            indexStatus = Int32.Parse(firstIndexStatus);

            firstDisseminationInterval = firstDisseminationInterval.Substring
                (2, firstDisseminationInterval.Length - 2);
            disseminationInterval = Int32.Parse(firstDisseminationInterval);
        }
    }
}