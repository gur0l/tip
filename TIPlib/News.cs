﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class News : TipMsg
    {
        public readonly int sourceSystem;
        public readonly int ID;
        public readonly int newsID;
        public readonly String timeExec;
        public readonly int newsObjectType;
        public readonly String messageSource;

        public readonly bool URLflag = false;
        public readonly String URL;

        public readonly String headline;
        public readonly int unchangedPaid;
        public readonly String text;
        public readonly int blockID;
        public readonly char lastBlock;

        public News(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstSourceSystem = result[1];
            string firstID = result[2];
            string firstNewsID = result[3];
            string firstTimeExec = result[4];
            string firstNewsObjectType = result[5];
            string firstMessageSource = result[6];
            string firstURL = null;

            int x = 0;

            if (input.Contains(";URL"))
            {
                x++;
                URLflag = true;

                firstURL = result[6 + x];

                URL = firstURL.Substring
                    (3, firstURL.Length - 3);
            }

            string firstHeadline = result[7 + x];
            string firstText = result[8 + x];
            string firstBlockID = result[9 + x];
            string firstLastBlock = result[10 + x];

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            firstNewsID = firstNewsID.Substring
                    (3, firstNewsID.Length - 3);
            newsID = Int32.Parse(firstNewsID);

            timeExec = firstTimeExec.TrimStart('t');

            var builder = new StringBuilder();
            int count = 0;
            foreach (var c in timeExec)
            {
                builder.Append(c);

                if (!(count >= 4))
                {
                    if ((++count % 2) == 0)
                    {
                        builder.Append(':');
                    }
                }
            }
            timeExec = builder.ToString();

            firstNewsObjectType = firstNewsObjectType.Substring
                    (3, firstNewsObjectType.Length - 3);
            newsObjectType = Int32.Parse(firstNewsObjectType);

            messageSource = firstMessageSource.Substring
                    (3, firstMessageSource.Length - 3);

            headline = firstHeadline.Substring
                    (3, firstHeadline.Length - 3);

            text = firstText.Substring
                    (3, firstText.Length - 3);

            firstBlockID = firstBlockID.Substring
                    (3, firstBlockID.Length - 3);
            blockID = Int32.Parse(firstBlockID);

            firstLastBlock = firstLastBlock.Substring
                    (3, firstLastBlock.Length - 3);
            lastBlock = char.Parse(firstLastBlock);
        }
    }
}