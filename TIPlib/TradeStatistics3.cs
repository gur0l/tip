﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class TradeStatistics3 : TipMsg
    {
        public readonly int ID;
        public readonly int sourceSystem;
        public readonly String timeExec;

        public readonly bool firstPriceFlag = false;
        public readonly double firstPrice;

        public readonly bool lastPriceFlag = false;
        public readonly double lastPrice;

        public readonly bool highPriceFlag = false;
        public readonly double highPrice;

        public readonly bool lowPriceFlag = false;
        public readonly double lowPrice;

        public readonly bool diffLastPriceFlag = false;
        public readonly double diffLastPrice;

        public readonly bool numberOfTradesFlag = false;
        public readonly int numberOfTrades;

        public readonly bool accumulatedVolumeFlag = false;
        public readonly double accumulatedVolume;

        public readonly bool reportedVolumeFlag = false;
        public readonly double reportedVolume;

        public readonly bool accumulatedTurnoverFlag = false;
        public readonly double accumulatedTurnover;

        public readonly bool reportedTurnoverFlag = false;
        public readonly double reportedTurnover;

        public readonly bool lastTradeReportPriceFlag = false;
        public readonly double lastTradeReportPrice;

        public readonly bool lastTradeReportQuantityFlag = false;
        public readonly double lastTradeReportQuantity;

        public readonly bool orderbookFlushFlag = false;
        public readonly bool orderbookFlush;

        public readonly bool VWAPFlag = false;
        public readonly double VWAP;

        public readonly bool VWAPDiffPerFlag = false;
        public readonly double VWAPDiffPer;

        public readonly bool numberOfTradeReportsFlag = false;
        public readonly int numberOfTradeReports;

        public readonly bool diffDayPerFlag = false;
        public readonly double diffDayPer;

        public readonly bool TWAPFlag = false;
        public readonly double TWAP;

        public readonly bool closingAuctionPriceFlag = false;
        public readonly double closingAuctionPrice;

        public readonly bool lastVolumeFlag = false;
        public readonly double lastVolume;

        public readonly bool remainingQuantityFlag = false;
        public readonly double remainingQuantity;

        public TradeStatistics3(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceSystem = result[2];
            string firstTimeExec = result[3];

            string firstFirstPrice = null;
            string firstLastPrice = null;
            string firstHighPrice = null;
            string firstLowPrice = null;
            string firstDiffLastPrice = null;
            string firstNumberOfTrades = null;
            string firstAccumulatedVolume = null;
            string firstReportedVolume = null;
            string firstAccumulatedTurnover = null;
            string firstReportedTurnover = null;
            string firstLastTradeReportPrice = null;
            string firstLastTradeReportQuantity = null;
            string firstVWAP = null;
            string firstVWAPDiffPer = null;
            string firstNumberOfTradeReports = null;
            string firstDiffDayPer = null;
            string firstTWAP = null;
            string firstClosingAuctionPrice = null;
            string firstLastVolume = null;
            string firstRemainingQuantity = null;

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            timeExec = firstTimeExec.TrimStart('t');

            var builder = new StringBuilder();
            int count = 0;
            foreach (var c in timeExec)
            {
                builder.Append(c);

                if (!(count >= 4))
                {
                    if ((++count % 2) == 0)
                    {
                        builder.Append(':');
                    }
                }
            }
            timeExec = builder.ToString();

            // TOMORROW IS WHEN THIS IS OVER.

            int x = 0;

            if (input.Contains(";Pf"))
            {
                x++;
                firstPriceFlag = true;

                firstFirstPrice = result[3 + x];

                firstFirstPrice = firstFirstPrice.Substring
                    (2, firstFirstPrice.Length - 2);

                firstPrice = double.Parse
                (
                    firstFirstPrice, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            if (input.Contains(";Pl"))
            {
                x++;
                lastPriceFlag = true;

                firstLastPrice = result[3 + x];

                firstLastPrice = firstLastPrice.Substring
                    (2, firstLastPrice.Length - 2);

                lastPrice = double.Parse
                (
                    firstLastPrice, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            if (input.Contains(";Ph"))
            {
                x++;
                highPriceFlag = true;

                firstHighPrice = result[3 + x];

                firstHighPrice = firstHighPrice.Substring
                    (2, firstHighPrice.Length - 2);

                highPrice = double.Parse
                (
                    firstHighPrice, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            if (input.Contains(";LOp"))
            {
                x++;
                lowPriceFlag = true;

                firstLowPrice = result[3 + x];

                firstLowPrice = firstLowPrice.Substring
                    (3, firstLowPrice.Length - 3);

                lowPrice = double.Parse
                (
                    firstLowPrice, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            if (input.Contains(";Pd"))
            {
                x++;
                diffLastPriceFlag = true;

                firstDiffLastPrice = result[3 + x];

                firstDiffLastPrice = firstDiffLastPrice.Substring
                (2, firstDiffLastPrice.Length - 2);

                diffLastPrice = double.Parse
                (
                    firstDiffLastPrice, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            if (input.Contains(";q"))
            {
                x++;
                numberOfTradesFlag = true;

                firstNumberOfTrades = result[3 + x];

                firstNumberOfTrades = firstNumberOfTrades.TrimStart('q');

                numberOfTrades = Int32.Parse(firstNumberOfTrades);
            }

            if (input.Contains(";o"))
            {
                x++;
                accumulatedVolumeFlag = true;

                firstAccumulatedVolume = result[3 + x];

                firstAccumulatedVolume = firstAccumulatedVolume.TrimStart('o');

                accumulatedVolume = double.Parse
                (
                    firstAccumulatedVolume, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            if (input.Contains(";Rq"))
            {
                x++;
                reportedVolumeFlag = true;

                firstReportedVolume = result[3 + x];

                firstReportedVolume = firstReportedVolume.Substring
                    (2, firstReportedVolume.Length - 2);

                reportedVolume = double.Parse
                (
                    firstReportedVolume, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            if (input.Contains(";f"))
            {
                x++;
                accumulatedTurnoverFlag = true;

                firstAccumulatedTurnover = result[3 + x];

                firstAccumulatedTurnover = firstAccumulatedTurnover.TrimStart('f');

                accumulatedTurnover = double.Parse
                (
                    firstAccumulatedTurnover, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            if (input.Contains(";Rt"))
            {
                x++;
                reportedTurnoverFlag = true;

                firstReportedTurnover = result[3 + x];

                firstReportedTurnover = firstReportedTurnover.Substring
                (2, firstReportedTurnover.Length - 2);

                reportedTurnover = double.Parse
                (
                    firstReportedTurnover, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            if (input.Contains(";LTRp"))
            {
                x++;
                lastTradeReportPriceFlag = true;

                firstLastTradeReportPrice = result[3 + x];

                firstLastTradeReportPrice = firstLastTradeReportPrice.
                    Substring(4, firstLastTradeReportPrice.Length - 4);

                lastTradeReportPrice = double.Parse
                (
                    firstLastTradeReportPrice, 
                    CultureInfo.InvariantCulture.NumberFormat
                );
            }

            if (input.Contains(";LTRq"))
            {
                x++;
                lastTradeReportQuantityFlag = true;

                firstLastTradeReportQuantity = result[3 + x];

                firstLastTradeReportQuantity = firstLastTradeReportQuantity.
                    Substring(4, firstLastTradeReportQuantity.Length - 4);

                lastTradeReportQuantity = double.Parse
                (
                     firstLastTradeReportQuantity, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            if (input.Contains(";Of"))
            {
                x++;
                orderbookFlushFlag = true;
                orderbookFlush = true;
            }

            if (input.Contains(";Wp"))
            {
                x++;
                VWAPFlag = true;

                firstVWAP = result[3 + x];

                firstVWAP = firstVWAP.
                    Substring(2, firstVWAP.Length - 2);

                VWAP = double.Parse
                (
                    firstVWAP,
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            if (input.Contains(";Wd"))
            {
                x++;
                VWAPDiffPerFlag = true;

                firstVWAPDiffPer = result[3 + x];

                firstVWAPDiffPer = firstVWAPDiffPer.Substring
                    (2, firstVWAPDiffPer.Length - 2);

                VWAPDiffPer = double.Parse
                (
                    firstVWAPDiffPer, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            if (input.Contains(";Qr"))
            {
                x++;
                numberOfTradeReportsFlag = true;

                firstNumberOfTradeReports = result[3 + x];

                firstNumberOfTradeReports = firstNumberOfTradeReports.
                    Substring(2, firstNumberOfTradeReports.Length - 2);

                numberOfTradeReports = Int32.Parse(firstNumberOfTradeReports);
            }

            if (input.Contains(";Dd"))
            {
                x++;
                diffDayPerFlag = true;

                firstDiffDayPer = result[3 + x];

                firstDiffDayPer = firstDiffDayPer.
                    Substring(2, firstDiffDayPer.Length - 2);

                diffDayPer = double.Parse
                (
                    firstDiffDayPer,
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            if (input.Contains(";Tp"))
            {
                x++;
                TWAPFlag = true;

                firstTWAP = result[3 + x];

                firstTWAP = firstTWAP.Substring(2, firstTWAP.Length - 2);

                TWAP = double.Parse
                (
                    firstTWAP,
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            if (input.Contains(";CLp"))
            {
                x++;
                closingAuctionPriceFlag = true;

                firstClosingAuctionPrice = result[3 + x];

                firstClosingAuctionPrice = firstClosingAuctionPrice.Substring
                (3, firstClosingAuctionPrice.Length - 3);

                closingAuctionPrice = double.Parse
                (
                    firstClosingAuctionPrice, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            if (input.Contains(";Lv"))
            {
                x++;
                lastVolumeFlag = true;

                firstLastVolume = result[3 + x];

                firstLastVolume = firstLastVolume.Substring
                (2, firstLastVolume.Length - 2);

                lastVolume = double.Parse
                (
                    firstLastVolume,
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            if (input.Contains(";AQs"))
            {
                x++;
                remainingQuantityFlag = true;

                firstRemainingQuantity = result[3 + x];

                firstRemainingQuantity = firstRemainingQuantity.Substring
                (3, firstRemainingQuantity.Length - 3);

                remainingQuantity = double.Parse
                (
                    firstRemainingQuantity,
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }
        }
    }
}