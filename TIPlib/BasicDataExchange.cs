﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class BasicDataExchange : TipMsg
    {
        public readonly int ID;
        public readonly String sourceID;
        public readonly int sourceSystem;
        public readonly String symbol;
        public readonly String name;
        public readonly String country;
        public readonly bool micFlag = false;
        public readonly String micCode;

        public BasicDataExchange(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceID = result[2];
            string firstSourceSystem = result[3];
            string firstSymbol = result[4];
            string firstName = result[5];
            string firstCountry = result[6];
            string firstMicCode = null;

            if (input.Contains(";MIc"))
            {
                micFlag = true;

                firstMicCode = result[7];

                micCode = firstMicCode.Substring
                (
                    3, firstMicCode.Length - 3
                );
            }

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            sourceID = firstSourceID.Substring(2, firstSourceID.Length - 2);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            symbol = firstSymbol.Substring
            (
                3, firstSymbol.Length - 3
            );

            name = firstName.Substring(3, firstName.Length - 3);

            country = firstCountry.Substring(3, firstCountry.Length - 3);
        }
    }
}