﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class BasicDataIndexMember : TipMsg
    {
        public readonly int orderBookID;
        public readonly int indexID;
        public readonly int sourceSystem;

        public BasicDataIndexMember(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstOrderBookID = result[1];
            string firstIndexID = result[2];
            string firstSourceSystem = result[3];

            // TOMORROW IS WHEN THIS IS OVER.

            firstOrderBookID = firstOrderBookID.Substring
                (3, firstOrderBookID.Length - 3);
            orderBookID = Int32.Parse(firstOrderBookID);

            firstIndexID = firstIndexID.Substring
                (3, firstIndexID.Length - 3);
            indexID = Int32.Parse(firstIndexID);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);
        }
    }
}