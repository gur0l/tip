﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class BasicDataTickSizeEntry : TipMsg
    {
        public readonly int ID;
        public readonly String sourceID;
        public readonly int sourceSystem;
        public readonly double tickSize;
        public readonly double priceFrom;
        public readonly double priceTo;

        public BasicDataTickSizeEntry(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceID = result[2];
            string firstSourceSystem = result[3];
            string firstTickSize = result[4];
            string firstPriceFrom = result[5];
            string firstPriceTo = result[6];

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            sourceID = firstSourceID.Substring(2, firstSourceID.Length - 2);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            firstTickSize = firstTickSize.Substring
            (
                3, firstTickSize.Length - 3
            );
            tickSize = double.Parse
            (
                firstTickSize, CultureInfo.InvariantCulture.NumberFormat
            );

            firstPriceFrom = firstPriceFrom.Substring
            (
                3, firstPriceFrom.Length - 3
            );
            priceFrom = double.Parse
            (
                firstPriceFrom, CultureInfo.InvariantCulture.NumberFormat
            );

            firstPriceTo = firstPriceTo.Substring
            (
                3, firstPriceTo.Length - 3
            );
            priceTo = double.Parse
            (
                firstPriceTo, CultureInfo.InvariantCulture.NumberFormat
            );
        }
    }
}