﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class BasicDataShare : TipMsg
    {
        public readonly int ID;
        public readonly String sourceID;
        public readonly int sourceSystem;

        public readonly bool hotInsertedFlag = false;
        public readonly bool hotInserted;

        public readonly bool availableQuantityAtStartFlag = false;
        public readonly double availableQuantityAtStart;

        public readonly bool availableQtyStartDateFlag = false;
        public readonly DateTime availableQtyStartDate;

        public readonly bool availableQtyEndDateFlag = false;
        public readonly DateTime availableQtyEndDate;

        public readonly bool instrumentClassificationFlag = false;
        public readonly int instrumentClassification;

        public readonly bool totalIssueFlag = false;
        public readonly double totalIssue;

        public BasicDataShare(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceID = result[2];
            string firstSourceSystem = result[3];
            string firstHotInserted = null;

            int x = 0;

            if (input.Contains(";HOt"))
            {
                x++;
                hotInsertedFlag = true;
                hotInserted = true;
            }

            string firstAvailableQuantityAtStart = null;

            if (input.Contains(";AVq"))
            {
                x++;
                availableQuantityAtStartFlag = true;

                firstAvailableQuantityAtStart = result[3 + x];

                firstAvailableQuantityAtStart =
                    firstAvailableQuantityAtStart.Substring
                    (3, firstAvailableQuantityAtStart.Length - 3);
                availableQuantityAtStart = double.Parse
                    (firstAvailableQuantityAtStart,
                    CultureInfo.InvariantCulture.NumberFormat);
            }

            string firstAvailableQtyStartDate = null;

            if (input.Contains(";As"))
            {
                x++;
                availableQtyStartDateFlag = true;

                firstAvailableQtyStartDate = result[3 + x];

                firstAvailableQtyStartDate =
                    firstAvailableQtyStartDate.Substring
                    (2, firstAvailableQtyStartDate.Length - 2);

                availableQtyStartDate = DateTime.ParseExact
                (
                    firstAvailableQtyStartDate, "yyyyMMdd",
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            string firstAvailableQtyEndDate = null;

            if (input.Contains(";Ae"))
            {
                x++;
                availableQtyEndDateFlag = true;

                firstAvailableQtyEndDate = result[3 + x];

                firstAvailableQtyEndDate =
                    firstAvailableQtyEndDate.Substring
                    (2, firstAvailableQtyEndDate.Length - 2);

                availableQtyEndDate = DateTime.ParseExact
                (
                    firstAvailableQtyEndDate, "yyyyMMdd",
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            string firstInstrumentClassification = null;

            if (input.Contains(";ICl"))
            {
                x++;
                instrumentClassificationFlag = true;

                firstInstrumentClassification = result[3 + x];

                firstInstrumentClassification =
                    firstInstrumentClassification.Substring
                    (3, firstInstrumentClassification.Length - 3);

                instrumentClassification = Int32.Parse
                    (firstInstrumentClassification);
            }

            string firstTotalIssue = null;

            if (input.Contains(";TIs"))
            {
                x++;
                availableQuantityAtStartFlag = true;

                firstTotalIssue = result[3 + x];

                firstTotalIssue =
                    firstTotalIssue.Substring
                    (3, firstTotalIssue.Length - 3);

                totalIssue = double.Parse
                    (firstTotalIssue,
                    CultureInfo.InvariantCulture.NumberFormat);
            }

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            sourceID = firstSourceID.Substring(2, firstSourceID.Length - 2);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);
        }
    }
}