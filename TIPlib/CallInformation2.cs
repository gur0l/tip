﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class CallInformation2 : TipMsg
    {
        // TradedThroughDate, InstrumentExternalText
        // NominalValue, ClearingVenueId, NoOfSettlementDays
        // HotInserted, ShortSellValidation, MarketMaker.

        public readonly int ID;
        public readonly int sourceSystem;
        public readonly String timeExec;
        public readonly double equilibriumPrice;
        public readonly double equilibriumVolume;
        public readonly double remainingBidVolumeAtEPLevel;
        public readonly double remainingAskVolumeAtEPLevel;

        public readonly bool orderbookFlushFlag;
        public readonly bool orderbookFlush;

        public CallInformation2(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceSystem = result[2];
            string firstTimeExec = result[3];
            string firstEquilibriumPrice = result[4];
            string firstEquilibriumVolume = result[5];
            string firstRemainingBidVolumeAtEPLevel = result[6];
            string firstRemainingAskVolumeAtEPLevel = result[7];

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            timeExec = firstTimeExec.TrimStart('t');

            var builder = new StringBuilder();
            int count = 0;
            foreach (var c in timeExec)
            {
                builder.Append(c);

                if (!(count >= 4))
                {
                    if ((++count % 2) == 0)
                    {
                        builder.Append(':');
                    }
                }
            }
            timeExec = builder.ToString();

            firstEquilibriumPrice = firstEquilibriumPrice.Substring
                (3, firstEquilibriumPrice.Length - 3);
            equilibriumPrice = double.Parse
                (firstEquilibriumPrice, CultureInfo.InvariantCulture.NumberFormat);

            firstEquilibriumVolume = firstEquilibriumVolume.Substring
                (3, firstEquilibriumVolume.Length - 3);
            equilibriumVolume = double.Parse
                (firstEquilibriumVolume, CultureInfo.InvariantCulture.NumberFormat);

            firstRemainingBidVolumeAtEPLevel = firstRemainingBidVolumeAtEPLevel.Substring
                (3, firstRemainingBidVolumeAtEPLevel.Length - 3);
            remainingBidVolumeAtEPLevel = double.Parse
                (firstRemainingBidVolumeAtEPLevel, CultureInfo.InvariantCulture.NumberFormat);

            firstRemainingAskVolumeAtEPLevel = firstRemainingAskVolumeAtEPLevel.Substring
                (3, firstRemainingAskVolumeAtEPLevel.Length - 3);
            remainingAskVolumeAtEPLevel = double.Parse
                (firstRemainingAskVolumeAtEPLevel, CultureInfo.InvariantCulture.NumberFormat);

            if (input.Contains(";Of"))
            {
                orderbookFlushFlag = true;
                orderbookFlush = true;
            }
        }
    }
}