﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class BasicDataMarket : TipMsg
    {
        public readonly int ID;
        public readonly String sourceID;
        public readonly int sourceSystem;
        public readonly int exchangeID;
        public readonly String symbol;
        public readonly String name;
        public readonly String timeOffsetUTC;
        public readonly DateTime listingDate;
        public readonly bool tradedThroughDateFlag = false;
        public readonly DateTime tradedThroughDate;

        public BasicDataMarket(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceID = result[2];
            string firstSourceSystem = result[3];
            string firstExchangeID = result[4];
            string firstName = result[5];
            string firstSymbol = result[6];
            string firstTimeOffsetUTC = result[7];
            string firstListingDate = result[8];
            string firstTradedThroughDate = null;

            if (input.Contains(";TTd"))
            {
                tradedThroughDateFlag = true;

                firstTradedThroughDate = result[9];

                firstTradedThroughDate = firstTradedThroughDate.Substring
                (
                    3, firstTradedThroughDate.Length - 3
                );

                tradedThroughDate = DateTime.ParseExact
                (
                    firstTradedThroughDate, "yyyyMMdd",
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            sourceID = firstSourceID.Substring(2, firstSourceID.Length - 2);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            firstExchangeID = firstExchangeID.Substring
            (
                2, firstExchangeID.Length - 2
            );
            exchangeID = Int32.Parse(firstExchangeID);

            name = firstName.Substring(3, firstName.Length - 3);

            symbol = firstSymbol.Substring
            (
                3, firstSymbol.Length - 3
            );

            timeOffsetUTC = firstTimeOffsetUTC.Substring
            (
                4, firstTimeOffsetUTC.Length - 4
            );

            firstListingDate = firstListingDate.Substring
            (
                3, firstListingDate.Length - 3
            );

            listingDate = DateTime.ParseExact
            (
                firstListingDate, "yyyyMMdd",
                System.Globalization.CultureInfo.InvariantCulture
            );
        }
    }
}