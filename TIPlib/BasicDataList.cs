﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class BasicDataList : TipMsg
    {
        public readonly int ID;
        public readonly String sourceID;
        public readonly int sourceSystem;
        public readonly bool parentIDflag = false;
        public readonly int parentID;

        public readonly bool symbolFlag = false;
        public readonly String symbol;

        public readonly String name;
        public readonly String currency;
        public readonly char TurnoverCalculationEnabled;

        //"BDLi;i12;Si255;s1;NAmSYSTEM;LCyTRY;TCeY;"

        public BasicDataList(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceID = result[2];
            string firstSourceSystem = result[3];
            string firstParentID = null;

            int x = 0;

            if (input.Contains(";PAi"))
            {
                x++;
                parentIDflag = true;

                firstParentID = result[3 + x];

                firstParentID = firstParentID.Substring
                (
                    3, firstParentID.Length - 3
                );
                parentID = Int32.Parse(firstParentID);
            }

            string firstSymbol = null;

            if (input.Contains(";SYm"))
            {
                x++;
                symbolFlag = true;

                firstSymbol = result[3 + x];

                symbol = firstSymbol.Substring
                (
                    3, firstSymbol.Length - 3
                );
            }

            string firstName = result[4 + x];
            string firstCurrency = result[5 + x];
            string firstTCE = result[6 + x];

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            sourceID = firstSourceID.Substring
            (
                2, firstSourceID.Length - 2
            );

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            name = firstName.Substring
            (
                3, firstName.Length - 3
            );

            currency = firstCurrency.Substring
            (
                3, firstCurrency.Length - 3
            );

            firstTCE = firstTCE.Substring
            (
                3, firstTCE.Length - 3
            );
            TurnoverCalculationEnabled = char.Parse(firstTCE);
        }
    }
}