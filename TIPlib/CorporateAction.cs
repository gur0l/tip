﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class CorporateAction : TipMsg
    {
        public readonly int ID;
        public readonly int sourceSystem;
        public readonly int actionStatus;
        public readonly int noteCode;
        public readonly String timeExec;

        public CorporateAction(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceSystem = result[2];
            string firstActionStatus = result[3];
            string firstNoteCode = result[4];
            string firstTimeExec = result[5];

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            firstActionStatus = firstActionStatus.Substring
                    (3, firstActionStatus.Length - 3);
            actionStatus = Int32.Parse(firstActionStatus);

            firstNoteCode = firstNoteCode.Substring
                    (3, firstNoteCode.Length - 3);
            noteCode = Int32.Parse(firstNoteCode);

            timeExec = firstTimeExec.TrimStart('t');

            var builder = new StringBuilder();
            int count = 0;
            foreach (var c in timeExec)
            {
                builder.Append(c);

                if (!(count >= 4))
                {
                    if ((++count % 2) == 0)
                    {
                        builder.Append(':');
                    }
                }
            }
            timeExec = builder.ToString();
        }
    }
}