﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class Trade : TipMsg
    {
        // TradedThroughDate, InstrumentExternalText
        // NominalValue, ClearingVenueId, NoOfSettlementDays
        // HotInserted, ShortSellValidation, MarketMaker.

        public readonly int ID;
        public readonly int sourceSystem;
        public readonly int tradeNumber;

        public readonly bool tradeCancelFlag = false;
        public readonly char tradeCancel;

        public readonly bool timeExecFlag = false;
        public readonly String timeExec;

        public readonly bool timestampAgreementFlag = false;
        public readonly String timestampAgreement;

        public readonly bool timestampTradeCancelFlag = false;
        public readonly String timestampTradeCancel;

        public readonly bool timestampDisseminationFlag = false;
        public readonly String timestampDissemination;

        public readonly bool settlementDateFlag = false;
        public readonly DateTime settlementDate;

        public readonly double price;
        public readonly double volume;

        public readonly bool tradeBuyerFlag = false;
        public readonly String tradeBuyer;

        public readonly bool tradeSellerFlag = false;
        public readonly String tradeSeller;

        public readonly bool tradeTypeFlag = false;
        public readonly int tradeType;

        public readonly bool tradeClassFlag = false;
        public readonly int tradeClass;

        public readonly char tradeUpdatesLastPaid;
        public readonly char tradeUpdatesHighLow;
        public readonly char tradeUpdatesTurnover;
        public readonly char latestTrade;

        public readonly bool dateExecFlag = false;
        public readonly DateTime dateExec;

        public readonly bool dateAgreementFlag = false;
        public readonly DateTime dateAgreement;

        public readonly bool dateDisseminationFlag = false;
        public readonly DateTime dateDissemination;

        public readonly bool dateTradeCancelFlag = false;
        public readonly DateTime dateTradeCancel;

        public readonly String tradeID;

        public readonly bool aggressivePartyFlag = false;
        public readonly char aggressiveParty;

        public Trade(string input)
        {
            string[] stringSeparators = new string[] { ";" };
            var builder = new StringBuilder();

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceSystem = result[2];
            string firstTradeNumber = result[3];
            string firstTradeCancel = null;

            int x = 0;
            int count = 0;

            if (input.Contains(";TCl"))
            {
                x++;
                tradeCancelFlag = true;

                firstTradeCancel = result[3 + x];

                firstTradeCancel = firstTradeCancel.Substring
                    (3, firstTradeCancel.Length - 3);

                tradeCancel = char.Parse(firstTradeCancel);
            }

            string firstTimeExec = null;

            if (input.Contains(";t"))
            {
                x++;
                timeExecFlag = true;

                firstTimeExec = result[3 + x];

                timeExec = firstTimeExec.TrimStart('t');

                builder = new StringBuilder();
                count = 0;
                foreach (var c in timeExec)
                {
                    builder.Append(c);

                    if (!(count >= 4))
                    {
                        if ((++count % 2) == 0)
                        {
                            builder.Append(':');
                        }
                    }
                }
                timeExec = builder.ToString();
            }

            string firstTimestampAgreement = null;
            string firstTimestampTradeCancel = null;

            if (input.Contains(";Ta"))
            {
                x++;
                timestampAgreementFlag = true;

                firstTimestampAgreement = result[3 + x];

                timestampAgreement = firstTimestampAgreement.Substring
                    (2, firstTimestampAgreement.Length - 2);

                builder = new StringBuilder();
                count = 0;
                foreach (var c in timestampAgreement)
                {
                    builder.Append(c);

                    if (!(count >= 4))
                    {
                        if ((++count % 2) == 0)
                        {
                            builder.Append(':');
                        }
                    }
                }
                timestampAgreement = builder.ToString();
            }

            if (input.Contains(";TCt"))
            {
                x++;
                timestampTradeCancelFlag = true;

                firstTimestampTradeCancel = result[3 + x];

                timestampTradeCancel = firstTimestampTradeCancel.Substring
                    (3, firstTimestampTradeCancel.Length - 3);

                builder = new StringBuilder();
                count = 0;
                foreach (var c in timestampTradeCancel)
                {
                    builder.Append(c);

                    if (!(count >= 4))
                    {
                        if ((++count % 2) == 0)
                        {
                            builder.Append(':');
                        }
                    }
                }
                timestampTradeCancel = builder.ToString();
            }

            string firstTimestampDissemination = null;

            if (input.Contains(";TDi"))
            {
                x++;
                timestampDisseminationFlag = true;

                firstTimestampDissemination = result[3 + x];

                timestampDissemination = firstTimestampDissemination.Substring
                    (3, firstTimestampDissemination.Length - 3);

                builder = new StringBuilder();
                count = 0;
                foreach (var c in timestampDissemination)
                {
                    builder.Append(c);

                    if (!(count >= 4))
                    {
                        if ((++count % 2) == 0)
                        {
                            builder.Append(':');
                        }
                    }
                }
                timestampDissemination = builder.ToString();
            }

            string firstSettlementDate = null;

            if (input.Contains(";Sd"))
            {
                x++;
                settlementDateFlag = true;

                firstSettlementDate = result[3 + x];

                firstSettlementDate = firstSettlementDate.Substring
                (3, firstSettlementDate.Length - 3);

                settlementDate = DateTime.ParseExact
                (
                    firstSettlementDate, "yyyyMMdd",
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            string firstPrice = result[4 + x];
            string firstVolume = result[5 + x];
            string firstTradeBuyer = null;
            string firstTradeSeller = null;
            string firstTradeType = null;
            string firstTradeClass = null;

            if (input.Contains(";Tb"))
            {
                x++;
                tradeBuyerFlag = true;

                firstTradeBuyer = result[5 + x];

                tradeBuyer = firstTradeBuyer.Substring
                    (2, firstTradeBuyer.Length - 2);
            }

            if (input.Contains(";Ts"))
            {
                x++;
                tradeSellerFlag = true;

                firstTradeSeller = result[5 + x];

                tradeSeller = firstTradeSeller.Substring
                    (2, firstTradeSeller.Length - 2);
            }

            if (input.Contains(";Tt"))
            {
                x++;
                tradeTypeFlag = true;

                firstTradeType = result[5 + x];

                firstTradeType = firstTradeType.Substring
                    (2, firstTradeType.Length - 2);

                tradeType = Int32.Parse(firstTradeType);
            }

            if (input.Contains(";Tc"))
            {
                x++;
                tradeClassFlag = true;

                firstTradeClass = result[5 + x];

                firstTradeClass = firstTradeClass.Substring
                    (2, firstTradeClass.Length - 2);

                tradeClass = Int32.Parse(firstTradeClass);
            }

            string firstTradeUpdatesLastPaid = result[6 + x];
            string firstTradeUpdatesHighLow = result[7 + x];
            string firstTradeUpdatesTurnover = result[8 + x];
            string firstLatestTrade = result[9 + x];
            string firstDateExec = null;
            string firstDateAgreement = null;
            string firstDateDissemination = null;
            string firstDateTradeCancel = null;

            if (input.Contains(";Dx"))
            {
                x++;
                dateExecFlag = true;

                firstDateExec = result[9 + x];

                firstDateExec = firstDateExec.Substring
                    (2, firstDateExec.Length - 2);

                dateExec = DateTime.ParseExact
                (
                    firstDateExec, "yyyyMMdd",
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            if (input.Contains(";Da"))
            {
                x++;
                dateAgreementFlag = true;

                firstDateAgreement = result[9 + x];

                firstDateAgreement = firstDateAgreement.Substring
                    (2, firstDateAgreement.Length - 2);

                dateAgreement = DateTime.ParseExact
                (
                    firstDateAgreement, "yyyyMMdd",
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            if (input.Contains(";DDi"))
            {
                x++;
                dateDisseminationFlag = true;

                firstDateDissemination = result[9 + x];

                firstDateDissemination = firstDateDissemination.Substring
                    (3, firstDateDissemination.Length - 3);

                dateDissemination = DateTime.ParseExact
                (
                    firstDateDissemination, "yyyyMMdd",
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            if (input.Contains(";DCt"))
            {
                x++;
                dateTradeCancelFlag = true;

                firstDateTradeCancel = result[9 + x];

                firstDateTradeCancel = firstDateTradeCancel.Substring
                    (3, firstDateTradeCancel.Length - 3);

                dateTradeCancel = DateTime.ParseExact
                (
                    firstDateTradeCancel, "yyyyMMdd",
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            string firstTradeID = result[10 + x];
            string firstAggressiveParty = null;

            if (input.Contains(";Ag"))
            {
                x++;
                aggressivePartyFlag = true;

                firstAggressiveParty = result[10 + x];

                firstAggressiveParty = firstAggressiveParty.Substring
                    (2, firstAggressiveParty.Length - 2);

                aggressiveParty = char.Parse(firstAggressiveParty);
            }

            // TOMORROW IS WHEN THIS IS OVER.

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            firstTradeNumber = firstTradeNumber.Substring
                (2, firstTradeNumber.Length - 2);
            tradeNumber = Int32.Parse(firstTradeNumber);

            firstPrice = firstPrice.TrimStart('p');
            price = double.Parse
                (firstPrice, CultureInfo.InvariantCulture.NumberFormat);

            firstVolume = firstVolume.TrimStart('v');
            volume = double.Parse
                (firstVolume, CultureInfo.InvariantCulture.NumberFormat);

            firstTradeUpdatesLastPaid = firstTradeUpdatesLastPaid.Substring
                (2, firstTradeUpdatesLastPaid.Length - 2);
            tradeUpdatesLastPaid = char.Parse(firstTradeUpdatesLastPaid);

            firstTradeUpdatesHighLow = firstTradeUpdatesHighLow.Substring
                (2, firstTradeUpdatesHighLow.Length - 2);
            tradeUpdatesHighLow = char.Parse(firstTradeUpdatesHighLow);

            firstTradeUpdatesTurnover = firstTradeUpdatesTurnover.Substring
                (2, firstTradeUpdatesTurnover.Length - 2);
            tradeUpdatesTurnover = char.Parse(firstTradeUpdatesTurnover);

            firstLatestTrade = firstLatestTrade.Substring
                (2, firstLatestTrade.Length - 2);
            latestTrade = char.Parse(firstLatestTrade);

            tradeID = firstTradeID.Substring
                (2, firstTradeID.Length - 2);
        }
    }
}