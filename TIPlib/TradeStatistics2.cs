﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class TradeStatistics2 : TipMsg
    {
        public readonly int ID;
        public readonly int sourceSystem;
        public readonly String timeExec;

        public readonly bool firstPriceFlag = false;
        public readonly double firstPrice;

        public readonly bool lastPriceFlag = false;
        public readonly double lastPrice;

        public readonly bool highPriceFlag = false;
        public readonly double highPrice;

        public readonly bool lowPriceFlag = false;
        public readonly double lowPrice;

        public readonly bool diffLastPriceFlag = false;
        public readonly double diffLastPrice;

        public readonly bool lastTradeReportPriceFlag = false;
        public readonly double lastTradeReportPrice;

        public readonly bool orderbookFlushFlag = false;
        public readonly bool orderbookFlush;

        public readonly bool VWAPFlag = false;
        public readonly double VWAP;

        public readonly bool VWAPDiffPerFlag = false;
        public readonly double VWAPDiffPer;

        public readonly bool diffDayPerFlag = false;
        public readonly double diffDayPer;

        public readonly bool closingAuctionPriceFlag = false;
        public readonly double closingAuctionPrice;

        public TradeStatistics2(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceSystem = result[2];
            string firstTimeExec = result[3];
            string firstFirstPrice = null;
            string firstLastPrice = null;
            string firstHighPrice = null;
            string firstLowPrice = null;
            string firstDiffLastPrice = null;
            string firstLastTradeReportPrice = null;
            string firstVWAP = null;
            string firstVWAPDiffPer = null;
            string firstDiffDayPer = null;
            string firstClosingAuctionPrice = null;

            int x = 0;

            if (input.Contains(";Pf"))
            {
                x++;
                firstPriceFlag = true;

                firstFirstPrice = result[3 + x];

                firstFirstPrice = firstFirstPrice.Substring
                (
                    2, firstFirstPrice.Length - 2
                );

                firstPrice = double.Parse
                (
                    firstFirstPrice,
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            if (input.Contains(";Pl"))
            {
                x++;
                lastPriceFlag = true;

                firstLastPrice = result[3 + x];

                firstLastPrice = firstLastPrice.Substring
                (
                    2, firstLastPrice.Length - 2
                );

                lastPrice = double.Parse
                (
                    firstLastPrice,
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            if (input.Contains(";Ph"))
            {
                x++;
                highPriceFlag = true;

                firstHighPrice = result[3 + x];

                firstHighPrice = firstHighPrice.Substring
                (
                    2, firstHighPrice.Length - 2
                );

                highPrice = double.Parse
                (
                    firstHighPrice,
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            if (input.Contains(";LOp"))
            {
                x++;
                lowPriceFlag = true;

                firstLowPrice = result[3 + x];

                firstLowPrice = firstLowPrice.Substring
                (
                    3, firstLowPrice.Length - 3
                );

                lowPrice = double.Parse
                (
                    firstLowPrice,
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            if (input.Contains(";Pd"))
            {
                x++;
                diffLastPriceFlag = true;

                firstDiffLastPrice = result[3 + x];

                firstDiffLastPrice = firstDiffLastPrice.Substring
                (
                    2, firstDiffLastPrice.Length - 2
                );

                diffLastPrice = double.Parse
                (
                    firstDiffLastPrice,
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            if (input.Contains(";LTRp"))
            {
                x++;
                lastTradeReportPriceFlag = true;

                firstLastTradeReportPrice = result[3 + x];

                firstLastTradeReportPrice = firstLastTradeReportPrice.Substring
                (
                    4, firstLastTradeReportPrice.Length - 4
                );

                lastTradeReportPrice = double.Parse
                (
                    firstLastTradeReportPrice,
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            if (input.Contains(";Of"))
            {
                x++;
                orderbookFlushFlag = true;
                orderbookFlush = true;
            }

            if (input.Contains(";Wp"))
            {
                x++;
                VWAPFlag = true;

                firstVWAP = result[3 + x];

                firstVWAP = firstVWAP.Substring
                (
                    2, firstVWAP.Length - 2
                );

                VWAP = double.Parse
                (
                    firstVWAP,
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            if (input.Contains(";Wd"))
            {
                x++;
                VWAPDiffPerFlag = true;

                firstVWAPDiffPer = result[3 + x];

                firstVWAPDiffPer = firstVWAPDiffPer.Substring
                (
                    2, firstVWAPDiffPer.Length - 2
                );

                VWAPDiffPer = double.Parse
                (
                    firstVWAPDiffPer,
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            if (input.Contains(";Dd"))
            {
                x++;
                diffDayPerFlag = true;

                firstDiffDayPer = result[3 + x];

                firstDiffDayPer = firstDiffDayPer.Substring
                (
                    2, firstDiffDayPer.Length - 2
                );

                diffDayPer = double.Parse
                (
                    firstDiffDayPer,
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            if (input.Contains(";Clp"))
            {
                x++;
                closingAuctionPriceFlag = true;

                firstClosingAuctionPrice = result[3 + x];

                firstClosingAuctionPrice = firstClosingAuctionPrice.Substring
                (
                    3, firstClosingAuctionPrice.Length - 3
                );

                closingAuctionPrice = double.Parse
                (
                    firstClosingAuctionPrice,
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            timeExec = firstTimeExec.TrimStart('t');

            var builder = new StringBuilder();
            int count = 0;
            foreach (var c in timeExec)
            {
                builder.Append(c);

                if (!(count >= 4))
                {
                    if ((++count % 2) == 0)
                    {
                        builder.Append(':');
                    }
                }
            }
            timeExec = builder.ToString();
        }
    }
}