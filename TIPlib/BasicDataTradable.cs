﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class BasicDataTradable : TipMsg
    {
        // TradedThroughDate, InstrumentExternalText
        // NominalValue, ClearingVenueId, NoOfSettlementDays
        // HotInserted, ShortSellValidation, MarketMaker.

        public readonly int ID;
        public readonly String sourceID;
        public readonly int sourceSystem;
        public readonly int marketID;
        public readonly String instrumentSourceID;
        public readonly String symbol;
        public readonly String name;

        public readonly bool abbreviatedNameFlag = false;
        public readonly String abbreviatedName;

        public readonly int issuerID;
        public readonly String issueCurrency;
        public readonly String tradingCurrency;
        public readonly int priceType;
        public readonly int volumeDimention;
        public readonly DateTime listingDate;

        public readonly bool tradedThroughDateFlag = false;
        public readonly DateTime tradedThroughDate;

        public readonly bool instrumentExternalTextFlag = false;
        public readonly String instrumentExternalText;

        public readonly bool nominalValueFlag = false;
        public readonly double nominalValue;

        public readonly int tickSizeTableID;
        public readonly int noOfDecPrice;
        public readonly int noOfDecTradePrice;

        public readonly bool clearingVenueIDFlag = false;
        public readonly int clearingVenueID;

        public readonly bool countryFlag = false;
        public readonly String country;

        public readonly bool noOfSettlementDaysFlag = false;
        public readonly int noOfSettlementDays;
        public readonly int securityType;

        public readonly char automatch;
        public readonly char tradeReportsAllowed;
        public readonly char preTradeAnonimity;
        public readonly int postTradeAnonimity;

        public readonly bool marketSegmentFlag = false;
        public readonly int marketSegment;

        public readonly bool hotInsertedFlag = false;
        public readonly bool hotInserted;

        public readonly double lotSize;

        public readonly bool shortSellValidationFlag = false;
        public readonly int shortSellValidation;

        public readonly bool minimumLotFlag = false;
        public readonly double minimumLot;

        public readonly bool maximumLotFlag = false;
        public readonly double maximumLot;

        public readonly int tradingSession;

        public readonly bool marketMakerFlag = false;
        public readonly int[] marketMaker = 
            new int[5] { 0, 0, 0, 0, 0 };

        public readonly bool tradingMethodFlag = false;
        public readonly String tradingMethod;

        public readonly char grossSettlement;

        public BasicDataTradable(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceID = result[2];
            string firstSourceSystem = result[3];
            string firstMarketID = result[4];
            string firstInstrumentSourceID = result[5];
            string firstSymbol = result[6];
            string firstName = result[7];
            string firstAbbreviatedName = null;

            int x = 0;

            if (input.Contains(";SNm"))
            {
                x++;
                abbreviatedNameFlag = true;

                firstAbbreviatedName = result[7 + x];

                abbreviatedName = firstAbbreviatedName.Substring
                (3, firstAbbreviatedName.Length - 3);
            }

            string firstIssuerId = result[8 + x];
            string firstIssueCurrency = result[9 + x];
            string firstTradingCurrency = result[10 + x];
            string firstPriceType = result[11 + x];
            string firstVolumeDimention = result[12 + x];
            string firstListingDate = result[13 + x];
            string firstTradedThroughDate = null;

            if (input.Contains(";TTd"))
            {
                x++;
                tradedThroughDateFlag = true;

                firstTradedThroughDate = result[13 + x];

                firstTradedThroughDate = firstTradedThroughDate.Substring
                (3, firstTradedThroughDate.Length - 3);

                tradedThroughDate = DateTime.ParseExact
                (
                    firstTradedThroughDate, "yyyyMMdd",
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            string firstInstrumentExternalText = null;

            if (input.Contains(";IEt"))
            {
                x++;
                instrumentExternalTextFlag = true;

                firstInstrumentExternalText = result[13 + x];

                instrumentExternalText = firstInstrumentExternalText.Substring
                (
                    3, firstInstrumentExternalText.Length - 3
                );
            }

            string firstNominalValue = null;

            if (input.Contains(";NMv"))
            {
                x++;
                nominalValueFlag = true;

                firstNominalValue = result[13 + x];

                firstNominalValue = firstNominalValue.Substring
                (
                    3, firstNominalValue.Length - 3
                );

                nominalValue = double.Parse
                (
                    firstNominalValue, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstTickSizeTableID = result[14 + x];
            string firstNoOfDecPrice = result[15 + x];
            string firstNoOfDecTradePrice = result[16 + x];
            string firstClearingVenueID = null;

            if (input.Contains(";CLId"))
            {
                x++;
                clearingVenueIDFlag = true;

                firstClearingVenueID = result[16 + x];

                firstClearingVenueID = firstClearingVenueID.Substring
                (
                    4, firstClearingVenueID.Length - 4
                );

                clearingVenueID = Int32.Parse(firstNominalValue);
            }

            string firstCountry = null;

            if (input.Contains(";CNy"))
            {
                x++;
                countryFlag = true;

                firstCountry = result[16 + x];

                country = firstCountry.Substring
                (
                    3, firstCountry.Length - 3
                );
            }

            string firstNoOfSettlementDays = null;

            if (input.Contains(";SSc"))
            {
                x++;
                noOfSettlementDaysFlag = true;

                firstNoOfSettlementDays = result[16 + x];

                firstNoOfSettlementDays = firstNoOfSettlementDays.Substring
                (
                    3, firstNoOfSettlementDays.Length - 3
                );

                noOfSettlementDays = Int32.Parse(firstNoOfSettlementDays);
            }

            string firstSecurityType = result[17 + x];
            string firstAutomatch = result[18 + x];
            string firstTradeReportsAllowed = result[19 + x];
            string firstPreTradeAnonimity = result[20 + x];
            string firstPostTradeAnonimity = result[21 + x];
            string firstMarketSegment = null;

            if (input.Contains(";MSe"))
            {
                x++;
                marketSegmentFlag = true;

                firstMarketSegment = result[21 + x];

                firstMarketSegment = firstMarketSegment.Substring
                (
                    3, firstMarketSegment.Length - 3
                );

                marketSegment = Int32.Parse(firstMarketSegment);
            }

            //string firstHotInserted = null;

            if (input.Contains(";HOt"))
            {
                x++;
                hotInsertedFlag = true;
                hotInserted = true;
            }

            string firstLotSize = result[22 + x];
            string firstShortSellValidation = null;

            if (input.Contains(";SSv"))
            {
                x++;
                shortSellValidationFlag = true;

                firstShortSellValidation = result[22 + x];

                firstShortSellValidation = firstShortSellValidation.Substring
                (
                    3, firstShortSellValidation.Length - 3
                );

                shortSellValidation = Int32.Parse(firstShortSellValidation);
            }

            string firstMinimumLot = null;

            if (input.Contains(";MLm"))
            {
                x++;
                minimumLotFlag = true;

                firstMinimumLot = result[22 + x];

                firstMinimumLot = firstMinimumLot.Substring
                (
                    3, firstMinimumLot.Length - 3
                );

                minimumLot = double.Parse
                    (firstMinimumLot, CultureInfo.InvariantCulture.NumberFormat);
            }

            string firstMaximumLot = null;

            if (input.Contains(";MLt"))
            {
                x++;
                maximumLotFlag = true;

                firstMaximumLot = result[22 + x];

                firstMaximumLot = firstMaximumLot.Substring
                (
                    3, firstMaximumLot.Length - 3
                );

               maximumLot = double.Parse
                    (firstMaximumLot, CultureInfo.InvariantCulture.NumberFormat);
            }

            string firstTradingSession = result[23 + x];
            string firstMarketMaker = null;

            if (input.Contains(";MMk"))
            {
                x++;
                marketMakerFlag = true;

                firstMarketMaker = result[23 + x];

                firstMarketMaker = firstMarketMaker.Substring
                (
                    3, firstMarketMaker.Length - 3
                );

                string[] values = firstMarketMaker.Split(',');

                int count = 0;
                foreach (var c in values)
                {
                    marketMaker[count] = Int32.Parse(values[count]);
                }
            }

            string firstTradingMethod = null;

            if (input.Contains(";TRm"))
            {
                x++;
                tradingMethodFlag = true;

                firstTradingMethod = result[23 + x];

                tradingMethod = firstTradingMethod.Substring
                    (3, firstTradingMethod.Length - 3);
            }

            string firstGrossSettlement = result[24 + x];

            for (int i = 0; i < result.Length; i++)
                Console.WriteLine(result[i]);

            // TOMORROW IS WHEN THIS IS OVER.

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            sourceID = firstSourceID.Substring(2, firstSourceID.Length - 2);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            firstMarketID = firstMarketID.Substring(2, firstMarketID.Length - 2);
            marketID = Int32.Parse(firstMarketID);

            instrumentSourceID = firstInstrumentSourceID.Substring
                (3, firstInstrumentSourceID.Length - 3);

            symbol = firstSymbol.Substring(3, firstSymbol.Length - 3);

            name = firstName.Substring(3, firstName.Length - 3);

            firstIssuerId = firstIssuerId.Substring(3, firstIssuerId.Length - 3);
            issuerID = Int32.Parse(firstIssuerId);

            issueCurrency = firstIssueCurrency.Substring
                (3, firstIssueCurrency.Length - 3);

            tradingCurrency = firstTradingCurrency.Substring
                (3, firstTradingCurrency.Length - 3);

            firstPriceType = firstPriceType.Substring
                (3, firstPriceType.Length - 3);
            priceType = Int32.Parse(firstPriceType);

            firstVolumeDimention = firstVolumeDimention.Substring
                (3, firstVolumeDimention.Length - 3);
            volumeDimention = Int32.Parse(firstVolumeDimention);

            firstListingDate = firstListingDate.Substring
                (3, firstListingDate.Length - 3);

            listingDate = DateTime.ParseExact
            (
                firstListingDate, "yyyyMMdd",
                System.Globalization.CultureInfo.InvariantCulture
            );

            firstTickSizeTableID = firstTickSizeTableID.Substring
                (4, firstTickSizeTableID.Length - 4);
            tickSizeTableID = Int32.Parse(firstTickSizeTableID);

            firstNoOfDecPrice = firstNoOfDecPrice.Substring
                (3, firstNoOfDecPrice.Length - 3);
            noOfDecPrice = Int32.Parse(firstNoOfDecPrice);

            firstNoOfDecTradePrice = firstNoOfDecTradePrice.Substring
                (4, firstNoOfDecTradePrice.Length - 4);
            noOfDecTradePrice = Int32.Parse(firstNoOfDecTradePrice);

            firstSecurityType = firstSecurityType.Substring
                (3, firstSecurityType.Length - 3);
            securityType = Int32.Parse(firstSecurityType);

            firstAutomatch = firstAutomatch.Substring
                (3, firstAutomatch.Length - 3);
            automatch = char.Parse(firstAutomatch);

            firstTradeReportsAllowed = firstTradeReportsAllowed.Substring
                (3, firstTradeReportsAllowed.Length - 3);
            tradeReportsAllowed = char.Parse(firstTradeReportsAllowed);

            firstPreTradeAnonimity = firstPreTradeAnonimity.Substring
                (3, firstPreTradeAnonimity.Length - 3);
            preTradeAnonimity = char.Parse(firstPreTradeAnonimity);

            firstPostTradeAnonimity = firstPostTradeAnonimity.Substring
                (3, firstPostTradeAnonimity.Length - 3);
            postTradeAnonimity = Int32.Parse(firstPostTradeAnonimity);

            firstLotSize = firstLotSize.Substring
                (3, firstLotSize.Length - 3);
            lotSize = double.Parse
                (firstLotSize, CultureInfo.InvariantCulture.NumberFormat);

            firstTradingSession = firstTradingSession.Substring
                (4, firstTradingSession.Length - 4);
            tradingSession = Int32.Parse(firstTradingSession);

            firstGrossSettlement = firstGrossSettlement.Substring
                (3, firstGrossSettlement.Length - 3);
            grossSettlement = char.Parse(firstGrossSettlement);
        }
    }
}