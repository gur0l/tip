﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class BasicDataSector : TipMsg
    {
        public readonly int ID;
        public readonly String sourceID;
        public readonly int sourceSystem;
        public readonly bool symbolFlag = false;
        public readonly String symbol;
        public readonly String name;
        public readonly int codeLevel;
        public readonly bool parentIDflag = false;
        public readonly int parentID;

        public BasicDataSector(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceID = result[2];
            string firstSourceSystem = result[3];
            string firstSymbol = null;

            int x = 0;

            if (input.Contains(";SYm"))
            {
                x = 1;
                symbolFlag = true;

                firstSymbol = result[4];

                symbol = firstSymbol.Substring
                (
                    3, firstSymbol.Length - 3
                );
            }

            string firstName = result[4 + x];
            string firstCodeLevel = result[5 + x];
            string firstParentID = null;

            if (input.Contains(";PAi"))
            {
                parentIDflag = true;

                firstParentID = result[6 + x];

                firstParentID = firstParentID.Substring
                (
                    3, firstParentID.Length - 3
                );
                parentID = Int32.Parse(firstParentID);
            }

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            sourceID = firstSourceID.Substring
            (
                2, firstSourceID.Length - 2
            );

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            name = firstName.Substring
            (
                3, firstName.Length - 3
            );

            firstCodeLevel = firstCodeLevel.Substring
            (
                4, firstCodeLevel.Length - 4
            );
            codeLevel = Int32.Parse(firstCodeLevel);
        }
    }
}