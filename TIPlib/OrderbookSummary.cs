﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class OrderbookSummary : TipMsg
    {
        public readonly int ID;
        public readonly int sourceSystem;
        public readonly String timeExec;
        public readonly DateTime date;
        public readonly char officialClosingPrice;
        public readonly char officialClosingTurnover;

        //rest is optional.
        public readonly bool bidPriceDiffFlag = false;
        public readonly double bidPriceDiff;

        public readonly bool bidPriceAtLevel1Flag = false;
        public readonly double bidPriceAtLevel1;

        public readonly bool askPriceAtLevel1Flag = false;
        public readonly double askPriceAtLevel1;

        public readonly bool firstPriceFlag = false;
        public readonly double firstPrice;

        public readonly bool lastPriceFlag = false;
        public readonly double lastPrice;

        public readonly bool highPriceFlag = false;
        public readonly double highPrice;

        public readonly bool lowPriceFlag = false;
        public readonly double lowPrice;

        public readonly bool diffLastPriceFlag = false;
        public readonly double diffLastPrice;

        public readonly bool numberOfTradesFlag = false;
        public readonly int numberOfTrades;

        public readonly bool accumulatedVolumeFlag = false;
        public readonly double accumulatedVolume;

        public readonly bool reportedVolumeFlag = false;
        public readonly double reportedVolume;

        public readonly bool accumulatedTurnoverFlag = false;
        public readonly double accumulatedTurnover;

        public readonly bool reportedTurnoverFlag = false;
        public readonly double reportedTurnover;

        public readonly bool settlementPriceFlag = false;
        public readonly double settlementPrice;

        public readonly bool highPriceMonthFlag = false;
        public readonly double highPriceMonth;

        public readonly bool highPriceMonthDateFlag = false;
        public readonly DateTime highPriceMonthDate;

        public readonly bool lowPriceMonthFlag = false;
        public readonly double lowPriceMonth;

        public readonly bool lowPriceMonthDateFlag = false;
        public readonly DateTime lowPriceMonthDate;

        public readonly bool highPriceYearFlag = false;
        public readonly double highPriceYear;

        public readonly bool highPriceYearDateFlag = false;
        public readonly DateTime highPriceYearDate;

        public readonly bool lowPriceYearFlag = false;
        public readonly double lowPriceYear;

        public readonly bool lowPriceYearDateFlag = false;
        public readonly DateTime lowPriceYearDate;

        public readonly bool lastTradedDateFlag = false;
        public readonly DateTime lastTradedDate;

        public readonly bool lastPaidDateFlag = false;
        public readonly DateTime lastPaidDate;

        public readonly bool closingVWAPFlag = false;
        public readonly double closingVWAP;

        public OrderbookSummary(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceSystem = result[2];
            string firstTimeExec = result[3];
            string firstDate = result[4];
            string firstOfficialClosingPrice = result[5];
            string firstOfficialClosingTurnover = result[6];

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            timeExec = firstTimeExec.TrimStart('t');

            var builder = new StringBuilder();
            int count = 0;
            foreach (var c in timeExec)
            {
                builder.Append(c);

                if (!(count >= 4))
                {
                    if ((++count % 2) == 0)
                    {
                        builder.Append(':');
                    }
                }
            }
            timeExec = builder.ToString();

            firstDate = firstDate.Substring
                (2, firstDate.Length - 2);

            date = DateTime.ParseExact
            (
                firstDate, "yyyyMMdd",
                System.Globalization.CultureInfo.InvariantCulture
            );

            firstOfficialClosingPrice = firstOfficialClosingPrice.Substring
                (4, firstOfficialClosingPrice.Length - 4);
            officialClosingPrice = char.Parse(firstOfficialClosingPrice);

            firstOfficialClosingTurnover = firstOfficialClosingTurnover.Substring
                (4, firstOfficialClosingTurnover.Length - 4);
            officialClosingTurnover = char.Parse(firstOfficialClosingTurnover);

            // TOMORROW IS WHEN THIS IS OVER.

            string firstBidPriceDiff = null;

            int x = 0;

            if (input.Contains(";d"))
            {
                x++;
                bidPriceDiffFlag = true;

                firstBidPriceDiff = result[6 + x];

                firstBidPriceDiff = firstBidPriceDiff.TrimStart('d');

                bidPriceDiff = double.Parse
                (
                    firstBidPriceDiff, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstBidPriceAtLevel1 = null;

            if (input.Contains(";BPr"))
            {
                x++;
                bidPriceAtLevel1Flag = true;

                firstBidPriceAtLevel1 = result[6 + x];

                firstBidPriceAtLevel1 = firstBidPriceAtLevel1.Substring
                (3, firstBidPriceAtLevel1.Length - 3);

                bidPriceAtLevel1 = double.Parse
                (
                    firstBidPriceAtLevel1, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstAskPriceAtLevel1 = null;

            if (input.Contains(";APl"))
            {
                x++;
                askPriceAtLevel1Flag = true;

                firstAskPriceAtLevel1 = result[6 + x];

                firstAskPriceAtLevel1 = firstAskPriceAtLevel1.Substring
                (3, firstAskPriceAtLevel1.Length - 3);

                askPriceAtLevel1 = double.Parse
                (
                    firstAskPriceAtLevel1, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstFirstPrice = null;

            if (input.Contains(";Pf"))
            {
                x++;
                firstPriceFlag = true;

                firstFirstPrice = result[6 + x];

                firstFirstPrice = firstFirstPrice.Substring
                (2, firstFirstPrice.Length - 2);

                firstPrice = double.Parse
                (
                    firstFirstPrice, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstLastPrice = null;

            if (input.Contains(";Pl"))
            {
                x++;
                lastPriceFlag = true;

                firstLastPrice = result[6 + x];

                firstLastPrice = firstLastPrice.Substring
                (2, firstLastPrice.Length - 2);

                lastPrice = double.Parse
                (
                    firstLastPrice, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstHighPrice = null;

            if (input.Contains(";Ph"))
            {
                x++;
                highPriceFlag = true;

                firstHighPrice = result[6 + x];

                firstHighPrice = firstHighPrice.Substring
                (2, firstHighPrice.Length - 2);

                highPrice = double.Parse
                (
                    firstHighPrice, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstLowPrice = null;

            if (input.Contains(";LOp"))
            {
                x++;
                lowPriceFlag = true;

                firstLowPrice = result[6 + x];

                firstLowPrice = firstLowPrice.Substring
                (3, firstLowPrice.Length - 3);

                lowPrice = double.Parse
                (
                    firstLowPrice, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstDiffLastPrice = null;

            if (input.Contains(";Pd"))
            {
                x++;
                diffLastPriceFlag = true;

                firstDiffLastPrice = result[6 + x];

                firstDiffLastPrice = firstDiffLastPrice.Substring
                (2, firstDiffLastPrice.Length - 2);

                diffLastPrice = double.Parse
                (
                    firstDiffLastPrice, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstNumberOfTrades = null;

            if (input.Contains(";q"))
            {
                x++;
                numberOfTradesFlag = true;

                firstNumberOfTrades = result[6 + x];

                firstNumberOfTrades = firstNumberOfTrades.TrimStart('q');

                numberOfTrades = Int32.Parse(firstNumberOfTrades);
            }

            string firstAccumulatedVolume = null;

            if (input.Contains(";o"))
            {
                x++;
                accumulatedVolumeFlag = true;

                firstAccumulatedVolume = result[6 + x];

                firstAccumulatedVolume = firstAccumulatedVolume.TrimStart('o');

                accumulatedVolume = double.Parse
                (
                    firstAccumulatedVolume, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstReportedVolume = null;

            if (input.Contains(";Rq"))
            {
                x++;
                reportedVolumeFlag = true;

                firstReportedVolume = result[6 + x];

                firstReportedVolume = firstReportedVolume.Substring
                (2, firstReportedVolume.Length - 2);

                reportedVolume = double.Parse
                (
                    firstReportedVolume, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstAccumulatedTurnover = null;

            if (input.Contains(";f"))
            {
                x++;
                accumulatedTurnoverFlag = true;

                firstAccumulatedTurnover = result[6 + x];

                firstAccumulatedTurnover = firstAccumulatedTurnover.TrimStart('f');

                accumulatedTurnover = double.Parse
                (
                    firstAccumulatedTurnover, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstReportedTurnover = null;

            if (input.Contains(";Rt"))
            {
                x++;
                reportedTurnoverFlag = true;

                firstReportedTurnover = result[6 + x];

                firstReportedTurnover = firstReportedTurnover.Substring
                (2, firstReportedTurnover.Length - 2);

                reportedTurnover = double.Parse
                (
                    firstReportedTurnover, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstSettlementPrice = null;

            if (input.Contains(";SEp"))
            {
                x++;
                settlementPriceFlag = true;

                firstSettlementPrice = result[6 + x];

                firstSettlementPrice = firstSettlementPrice.Substring
                (3, firstSettlementPrice.Length - 3);

                settlementPrice = double.Parse
                (
                    firstSettlementPrice, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstHighPriceMonth = null;

            if (input.Contains(";HPm"))
            {
                x++;
                highPriceMonthFlag = true;

                firstHighPriceMonth = result[6 + x];

                firstHighPriceMonth = firstHighPriceMonth.Substring
                (3, firstHighPriceMonth.Length - 3);

                highPriceMonth = double.Parse
                (
                    firstHighPriceMonth, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            //Tomorrow...
            string firstHighPriceMonthDate = null;

            if (input.Contains(";HPMd"))
            {
                x++;
                highPriceMonthDateFlag = true;

                firstHighPriceMonthDate = result[6 + x];

                firstHighPriceMonthDate = firstHighPriceMonthDate.Substring
                (4, firstHighPriceMonthDate.Length - 4);

                highPriceMonthDate = DateTime.ParseExact
                (
                    firstHighPriceMonthDate, "yyyyMMdd",
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            string firstLowPriceMonth = null;

            if (input.Contains(";LPm"))
            {
                x++;
                lowPriceMonthFlag = true;

                firstLowPriceMonth = result[6 + x];

                firstLowPriceMonth = firstLowPriceMonth.Substring
                (3, firstLowPriceMonth.Length - 3);

                lowPriceMonth = double.Parse
                (
                    firstLowPriceMonth, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstLowPriceMonthDate = null;

            if (input.Contains(";LPMd"))
            {
                x++;
                lowPriceMonthDateFlag = true;

                firstLowPriceMonthDate = result[6 + x];

                firstLowPriceMonthDate = firstLowPriceMonthDate.Substring
                (4, firstLowPriceMonthDate.Length - 4);

                lowPriceMonthDate = DateTime.ParseExact
                (
                    firstLowPriceMonthDate, "yyyyMMdd",
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            string firstHighPriceYear = null;

            if (input.Contains(";HPy"))
            {
                x++;
                highPriceYearFlag = true;

                firstHighPriceYear = result[6 + x];

                firstHighPriceYear = firstHighPriceYear.Substring
                (3, firstHighPriceYear.Length - 3);

                highPriceYear = double.Parse
                (
                    firstHighPriceYear, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstHighPriceYearDate = null;

            if (input.Contains(";HPYd"))
            {
                x++;
                highPriceYearDateFlag = true;

                firstHighPriceYearDate = result[6 + x];

                firstHighPriceYearDate = firstHighPriceYearDate.Substring
                (4, firstHighPriceYearDate.Length - 4);

                highPriceYearDate = DateTime.ParseExact
                (
                    firstHighPriceYearDate, "yyyyMMdd",
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            string firstLowPriceYear = null;

            if (input.Contains(";LPy"))
            {
                x++;
                lowPriceYearFlag = true;

                firstLowPriceYear = result[6 + x];

                firstLowPriceYear = firstLowPriceYear.Substring
                (3, firstLowPriceYear.Length - 3);

                lowPriceYear = double.Parse
                (
                    firstLowPriceYear, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstLowPriceYearDate = null;

            if (input.Contains(";LPYd"))
            {
                x++;
                lowPriceYearDateFlag = true;

                firstLowPriceYearDate = result[6 + x];

                firstLowPriceYearDate = firstLowPriceYearDate.Substring
                (4, firstLowPriceYearDate.Length - 4);

                lowPriceYearDate = DateTime.ParseExact
                (
                    firstLowPriceYearDate, "yyyyMMdd",
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            string firstLastTradedDate = null;

            if (input.Contains(";LTd"))
            {
                x++;
                lastTradedDateFlag = true;

                firstLastTradedDate = result[6 + x];

                firstLastTradedDate = firstLastTradedDate.Substring
                (3, firstLastTradedDate.Length - 3);

                lastTradedDate = DateTime.ParseExact
                (
                    firstLastTradedDate, "yyyyMMdd",
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            string firstLastPaidDate = null;

            if (input.Contains(";LPd"))
            {
                x++;
                lastPaidDateFlag = true;

                firstLastPaidDate = result[6 + x];

                firstLastPaidDate = firstLastPaidDate.Substring
                (3, firstLastPaidDate.Length - 3);

                lastPaidDate = DateTime.ParseExact
                (
                    firstLastPaidDate, "yyyyMMdd",
                    System.Globalization.CultureInfo.InvariantCulture
                );
            }

            string firstClosingVWAP = null;

            if (input.Contains(";CWp"))
            {
                x++;
                closingVWAPFlag = true;

                firstClosingVWAP = result[6 + x];

                firstClosingVWAP = firstClosingVWAP.Substring
                (3, firstClosingVWAP.Length - 3);

                closingVWAP = double.Parse
                (
                    firstClosingVWAP, CultureInfo.InvariantCulture.NumberFormat
                );
            }
        }
    }
}