﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class BasicDataTradingScheme : TipMsg
    {
        public readonly int sourceSystem;
        public readonly int tradingSession;
        public readonly String sourceID;
        public readonly DateTime date;
        public readonly int stateCode;

        //chose "String" because there is no construtor with
        //just time installed, 1/1/0001 gets assigned if you
        //use DateTime, which is wrong.
        public readonly String startTime;

        public BasicDataTradingScheme(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstSourceSystem = result[1];
            string firstTradingSession = result[2];
            string firstSourceID = result[3];
            string firstDate = result[4];
            string firstStateCode = result[5];
            string firstStartTime = result[6];

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            firstTradingSession = firstTradingSession.Substring
            (
                4, firstTradingSession.Length - 4
            );
            tradingSession = Int32.Parse(firstTradingSession);

            sourceID = firstSourceID.Substring(2, firstSourceID.Length - 2);

            firstDate = firstDate.Substring
            (
                2, firstDate.Length - 2
            );

            date = DateTime.ParseExact
            (
                firstDate, "yyyyMMdd",
                System.Globalization.CultureInfo.InvariantCulture
            );

            firstStateCode = firstStateCode.Substring
            (
                2, firstStateCode.Length - 2
            );
            stateCode = Int32.Parse(firstStateCode);

            startTime = firstStartTime.Substring
            (
                2, firstStartTime.Length - 2
            );

            //adding the 2 ":" characters to the time.
            var builder = new StringBuilder();
            int count = 0;
            foreach (var c in startTime)
            {
                builder.Append(c);

                if (!(count >= 4))
                {
                    if ((++count % 2) == 0)
                    {
                        builder.Append(':');
                    }
                }
            }
            startTime = builder.ToString();
        }
    }
}