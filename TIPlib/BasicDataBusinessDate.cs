﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class BasicDataBusinessDate : TipMsg
    {
        public readonly DateTime date;

        public BasicDataBusinessDate(string input)
        {
            string[] stringSeparators = new string[] { ";Dt" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstDate = result[1];

            firstDate = firstDate.Remove(firstDate.Length - 1);

            date = DateTime.ParseExact
            (
                firstDate, "yyyyMMdd",
                System.Globalization.CultureInfo.InvariantCulture
            );
        }
    }
}