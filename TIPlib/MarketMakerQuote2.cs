﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class MarketMakerQuote2 : TipMsg
    {
        // TradedThroughDate, InstrumentExternalText
        // NominalValue, ClearingVenueId, NoOfSettlementDays
        // HotInserted, ShortSellValidation, MarketMaker.

        public readonly int ID;
        public readonly int sourceSystem;
        public readonly String timeExec;

        public readonly bool askPriceFlag = false;
        public readonly double askPrice;

        public readonly bool bidPriceFlag = false;
        public readonly double bidPrice;

        public readonly bool bidVolumeFlag = false;
        public readonly double bidVolume;

        public readonly bool askVolumeFlag = false;
        public readonly double askVolume;

        public MarketMakerQuote2(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceSystem = result[2];
            string firstTimeExec = result[3];
            string firstAskPrice = null;
            string firstBidPrice = null;
            string firstBidVolume = null;
            string firstAskVolume = null;

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            timeExec = firstTimeExec.TrimStart('t');

            var builder = new StringBuilder();
            int count = 0;
            foreach (var c in timeExec)
            {
                builder.Append(c);

                if (!(count >= 4))
                {
                    if ((++count % 2) == 0)
                    {
                        builder.Append(':');
                    }
                }
            }
            timeExec = builder.ToString();

            int x = 0;

            if (input.Contains(";Pa"))
            {
                x++;
                askPriceFlag = true;

                firstAskPrice = result[3 + x];

                firstAskPrice = firstAskPrice.Substring
                    (2, firstAskPrice.Length - 2);
                askPrice = double.Parse
                    (firstAskPrice, CultureInfo.InvariantCulture.NumberFormat);
            }

            if (input.Contains(";Pb"))
            {
                x++;
                bidPriceFlag = true;

                firstBidPrice = result[3 + x];

                firstBidPrice = firstBidPrice.Substring
                    (2, firstBidPrice.Length - 2);
                bidPrice = double.Parse
                    (firstBidPrice, CultureInfo.InvariantCulture.NumberFormat);
            }

            if (input.Contains(";Vb"))
            {
                x++;
                bidVolumeFlag = true;

                firstBidVolume = result[3 + x];

                firstBidVolume = firstBidVolume.Substring
                    (2, firstBidVolume.Length - 2);
                bidVolume = double.Parse
                    (firstBidVolume, CultureInfo.InvariantCulture.NumberFormat);
            }

            if (input.Contains(";Va"))
            {
                x++;
                askVolumeFlag = true;

                firstAskVolume = result[3 + x];

                firstAskVolume = firstAskVolume.Substring
                    (2, firstAskVolume.Length - 2);
                askVolume = double.Parse
                    (firstAskVolume, CultureInfo.InvariantCulture.NumberFormat);
            }
        }
    }
}