﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIPlib
{
    public class IndexUpdate : TipMsg
    {
        public readonly int ID;
        public readonly int sourceSystem;
        public readonly String timeExec;
        public readonly double currentValue;

        public readonly bool highValueFlag = false;
        public readonly double highValue;

        public readonly bool lowValueFlag = false;
        public readonly double lowValue;

        public readonly double accumulatedVolume;
        public readonly double accumulatedTurnover;

        public readonly bool openValueFlag = false;
        public readonly double openValue;

        public readonly double diffDayNom;
        public readonly double diffDayPer;

        public IndexUpdate(string input)
        {
            string[] stringSeparators = new string[] { ";" };

            string[] result = input.Split(stringSeparators, StringSplitOptions.None);

            //string operation = result[0];
            string firstID = result[1];
            string firstSourceSystem = result[2];
            string firstTimeExec = result[3];
            string firstCurrentValue = result[4];

            firstCurrentValue = firstCurrentValue.Substring
                    (2, firstCurrentValue.Length - 2);

            currentValue = double.Parse
            (
                firstCurrentValue, CultureInfo.InvariantCulture.NumberFormat
            );

            int x = 0;

            string firstHighValue = null;

            if (input.Contains(";Vh"))
            {
                x++;
                highValueFlag = true;

                firstHighValue = result[4 + x];

                firstHighValue = firstHighValue.Substring
                    (2, firstHighValue.Length - 2);

                highValue = double.Parse
                (
                    firstHighValue, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstLowValue = null;

            if (input.Contains(";Vl"))
            {
                x++;
                lowValueFlag = true;

                firstLowValue = result[4 + x];

                firstLowValue = firstLowValue.Substring
                    (2, firstLowValue.Length - 2);

                lowValue = double.Parse
                (
                    firstLowValue, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstAccumulatedVolume = result[5 + x];
            string firstAccumulatedTurnover = result[6 + x];
            string firstOpenValue = null;

            if (input.Contains(";OVa"))
            {
                x++;
                openValueFlag = true;

                firstOpenValue = result[6 + x];

                firstOpenValue = firstOpenValue.Substring
                    (3, firstOpenValue.Length - 3);

                openValue = double.Parse
                (
                    firstOpenValue, CultureInfo.InvariantCulture.NumberFormat
                );
            }

            string firstDiffDayNom = result[7 + x];
            string firstDiffDayPer = result[8 + x];

            firstID = firstID.TrimStart('i');
            ID = Int32.Parse(firstID);

            firstSourceSystem = firstSourceSystem.TrimStart('s');
            sourceSystem = Int32.Parse(firstSourceSystem);

            timeExec = firstTimeExec.TrimStart('t');

            var builder = new StringBuilder();
            int count = 0;
            foreach (var c in timeExec)
            {
                builder.Append(c);

                if (!(count >= 4))
                {
                    if ((++count % 2) == 0)
                    {
                        builder.Append(':');
                    }
                }
            }
            timeExec = builder.ToString();

            firstAccumulatedVolume = firstAccumulatedVolume.TrimStart('o');
            accumulatedVolume = double.Parse
            (
                firstAccumulatedVolume, CultureInfo.InvariantCulture.NumberFormat
            );

            firstAccumulatedTurnover = firstAccumulatedTurnover.TrimStart('f');
            accumulatedTurnover = double.Parse
            (
                firstAccumulatedTurnover, CultureInfo.InvariantCulture.NumberFormat
            );

            firstDiffDayNom = firstDiffDayNom.Substring
                    (2, firstDiffDayNom.Length - 2);

            diffDayNom = double.Parse
            (
                firstDiffDayNom, CultureInfo.InvariantCulture.NumberFormat
            );

            firstDiffDayPer = firstDiffDayPer.Substring
                    (2, firstDiffDayPer.Length - 2);

            diffDayPer = double.Parse
            (
                firstDiffDayPer, CultureInfo.InvariantCulture.NumberFormat
            );
        }
    }
}